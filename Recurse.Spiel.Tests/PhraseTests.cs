﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using NUnit.Framework;
using Recurse.Spiel.English.Phrases;
using Recurse.Spiel.English.Phrases.Elements;
using Recurse.Spiel.Test;

namespace Recurse.Spiel.Tests
{
    public class PhraseTests : SpielTestFixture
    {
        [Test]
        public void RenderSimplePhrases()
        {
            Assert.AreEqual(
                "And then Ana and John were greeted by Paul.",
                RenderParagraph("And then " + new They(Ana, John) + " were greeted by " + new Them(Paul)));
        }

        [Test]
        public void RenderRecursivePhrases()
        {
            Assert.AreEqual(
                "You dance.",
                RenderParagraph(new They(You) + new Them(new Verb("dance"))));
        }

        [Test]
        public void RenderSimplePhrasesWithCollectivePronouns()
        {
            var input1 = "And then " + new They(Ana, John) + " were greeted by " + new Them(Paul);
            var input2 = "But " + new They(Ana, John) + " were mistrustful";

            var output = RenderParagraph(input1, input2);

            Assert.AreEqual("And then Ana and John were greeted by Paul. But they were mistrustful.", output);
        }

        [Test]
        public void RenderSimplePhrasesWithPronouns()
        {
            var input1 = "And then " + new They(Ana, John) + " were greeted by " + new Them(Paul);
            var input2 = "But " + new They(Ana) + " was mistrustful";

            var output = RenderParagraph(input1, input2);

            Assert.AreEqual("And then Ana and John were greeted by Paul. But she was mistrustful.", output);
        }

        [Test]
        public void ConjugateSimplePhrases()
        {
            var input1 = new They(Ana, John).Are + " satisfied";
            var input2 = new They(Paul).Are + " satisfied";

            Assert.AreEqual("Ana and John are satisfied.", RenderParagraph(input1));
            Assert.AreEqual("Paul is satisfied.", RenderParagraph(input2));
        }

        [Test]
        public void CanMergePredicates()
        {
            Assert.AreEqual(
                "near Ana and John",
                RenderObjects(
                    new Adjunct("near", Ana),
                    new Adjunct("near", John)));
        }

        [Test]
        public void CanMergeTwoPredicatePairs()
        {
            Assert.AreEqual(
                "near Ana and John and like him and Paul",
                RenderObjects(
                    new Adjunct("near", Ana),
                    new Adjunct("like", Paul),
                    new Adjunct("like", John),
                    new Adjunct("near", John)));
        }

        [Test]
        public void CanMergeMixedAdjuncts()
        {
            Assert.AreEqual(
                "near and like Ana and John",
                RenderObjects(
                    new Adjunct("near", Ana),
                    new Adjunct("like", Ana),
                    new Adjunct("near", John),
                    new Adjunct("like", John)));
        }

        [Test]
        public void UseReflexivePronouns()
        {
            Assert.AreEqual(
                "An apple ate itself.",
                RenderParagraph(new They(AnApple) + " ate " + new Them(AnApple)));
        }

        [Test]
        public void UseReflexivePronounsRecursively()
        {
            Assert.AreEqual(
                "John saw an apple resembling himself.",
                RenderParagraph(new They(John) + " saw " + new Them(AnApple) + new Adjunct("resembling", John)));
        }

        [Test]
        public void UseAliasesForSubjectsAppearingLaterInPhrase()
        {
            Assert.AreEqual(
                "Herself was the person Ana disliked.",
                RenderParagraph(new Them(Ana) + " was the person " + new They(Ana) + " disliked"));
        }

        [Test]
        public void AddSpacesBetweenPhraseElements()
        {
            Assert.AreEqual("Ana her.", RenderParagraph(new Them(Ana) + new Them(Ana)));
        }

        [Test]
        public void AddSpacesBetweenPhraseElementsAndStringsTreatedAsCompositePhraseElement()
        {
            SpielSubstring likesFlowers = "likes flowers";

            Assert.AreEqual("Ana likes flowers.", RenderParagraph(new Them(Ana) + likesFlowers));
            Assert.AreEqual("Likes flowers her.", RenderParagraph(likesFlowers + new Them(Ana)));
        }

        [Test]
        public void DoNotAddSpacesBetweenPhraseElementsAndStrings()
        {
            Assert.AreEqual("Ur-Ana.", RenderParagraph("Ur-" + new Them(Ana)));
            Assert.AreEqual("HerUr-.", RenderParagraph(new Them(Ana) + "Ur-"));
        }
    }
}
