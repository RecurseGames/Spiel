﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using NUnit.Framework;
using Recurse.Spiel.English.Phrases.Elements;
using Recurse.Spiel.Test;

namespace Recurse.Spiel.Tests
{
    public class PossessiveTests : SpielTestFixture
    {
        [Test]
        public void ApostropheAndSOnNamePossessive()
        {
            Assert.AreEqual("Ana's", RenderObjects(new Their(Ana)));
        }

        [Test]
        public void OnlyApostropheOnNamePossessiveEndingInS()
        {
            Assert.AreEqual("James'", RenderObjects(new Their(James)));
        }

        [Test]
        public void NoApostropheOnPossessivePronoun()
        {
            Assert.AreEqual("my", RenderObjects(new Their(Me)));
        }

        [Test]
        public void OnlyLastNameInListUsesPossessiveAddend()
        {
            Assert.AreEqual("Ana and Paul's", RenderObjects(new Their(Ana, Paul)));
        }
    }
}
