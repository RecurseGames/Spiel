﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using NUnit.Framework;
using Recurse.Spiel.Delimeters;
using Recurse.Spiel.English;
using Recurse.Spiel.Renderers;
using Recurse.Spiel.Renderings;
using Recurse.Spiel.Test.Helpers;
using System;

namespace Recurse.Spiel.Test
{
    public class SpielTestFixture
    {
        public DefaultRenderer Renderer { get; private set; }

        public TestEntity Ana { get; }

        public TestEntity AnApple { get; }

        public TestEntity John { get; }

        public TestEntity Paul { get; }

        public TestEntity You { get; }

        public TestEntity Me { get; }

        public TestEntity ScurvyEugene { get; }

        public TestEntity ScurvyStClaire { get; }

        public TestEntity James { get; }

        public SpielTestFixture()
        {
            You = new TestEntity(Pronoun.You);
            Me = new TestEntity(Pronoun.Hir, "FirstPerson");
            Ana = new TestEntity(Pronoun.She, "Ana");
            John = new TestEntity(Pronoun.He, "John");
            James = new TestEntity(Pronoun.He, "James");
            Paul = new TestEntity(Pronoun.He, "Paul");
            AnApple = new TestEntity(Pronoun.It, "an apple");
            ScurvyEugene = new TestEntity(Pronoun.He, "Scurvy", "Eugene");
            ScurvyStClaire = new TestEntity(Pronoun.He, "Scurvy", "St Claire");
        }

        [SetUp]
        public void SetUp()
        {
            Renderer = new DefaultRenderer();

            Renderer.AddAliasRendering(Pronoun.Me.CreateAlias(Me));
        }

        public string RenderParagraph(params SpielArgument[] sentences)
        {
            var writer = new SpielTextWriter();

            var renderedSentences = Renderer.RenderClauses(sentences);
            var delimitedSentences = SentenceDelimiter.Default.Delimit(renderedSentences);
            writer.Write(delimitedSentences);

            return writer.Text;
        }

        public string RenderObjects(params SpielArgument[] objects)
        {
            var writer = new SpielTextWriter();

            var renderer = Renderer.CreateObjectRenderer(objects);
            var objectList = SpielArgumentList.CreateNonEmpty(SpielArgumentListType.Collection, objects)
                ?? throw new ArgumentException("At least 1 object required.", nameof(objects));
            var renderedObjects = renderer.GetTransformedArguments(objectList);
            var renderedObjectList = ListDelimiter.OxfordComma.Delimit(renderedObjects);

            writer.Write(renderedObjectList);

            return writer.Text;
        }
    }
}
