﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Spiel.Aliases;
using Recurse.Spiel.English;
using Recurse.Spiel.English.Aliases;
using Recurse.Spiel.Renderers;

namespace Recurse.Spiel.Test.Helpers
{
    public class TestEntity : IIdentified, ISpielArgumentValue, IObject
    {
        public IAlias Pronouns { get; }

        public SpielArgument Identity { get; }

        public TestEntity(Pronoun pronouns, string firstName, string lastName)
        {
            Pronouns = pronouns;
            Identity = new Identity(this, firstName, lastName)
            {
                AliasFactories = { pronouns }
            };
        }

        public TestEntity(Pronoun pronouns, string name)
        {
            Pronouns = pronouns;
            Identity = new Identity(this, name)
            {
                AliasFactories = { pronouns }
            };
        }

        public TestEntity(Pronoun pronouns)
        {
            Pronouns = pronouns;
            Identity = new Identity(this) 
            {
                AliasFactories = { pronouns }
            };
        }

        public string ToRenderString()
        {
            return Identity.ToRenderedString();
        }

        public override string ToString()
        {
            return Identity.ToRenderedString();
        }

        public static implicit operator SpielArgument(TestEntity testArgument)
        {
            return new SpielArgument(testArgument);
        }
    }
}
