﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using NUnit.Framework;
using Recurse.Spiel.Test;

namespace Recurse.Spiel.Tests
{
    public class OxfordCommaTests : SpielTestFixture
    {
        [Test]
        public void RenderSingleItem()
        {
            Assert.AreEqual("Ana", RenderObjects(Ana));
        }

        [Test]
        public void RenderPair()
        {
            Assert.AreEqual("Ana and John", RenderObjects(Ana, John));
        }

        [Test]
        public void RenderThreeItems()
        {
            Assert.AreEqual("Ana, Paul, and John", RenderObjects(Ana, Paul, John));
        }

        [Test]
        public void RenderFourItems()
        {
            Assert.AreEqual("Ana, an apple, Paul, and John", RenderObjects(Ana, AnApple, Paul, John));
        }
    }
}
