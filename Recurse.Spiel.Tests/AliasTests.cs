﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using NUnit.Framework;
using Recurse.Spiel.English;
using Recurse.Spiel.English.Phrases.Elements;
using Recurse.Spiel.Test;
using Recurse.Spiel.Test.Helpers;

namespace Recurse.Spiel.Tests
{
    public class AliasTests : SpielTestFixture
    {
        [Test]
        public void RenderMostSpecificAliasFirst()
        {
            Assert.AreEqual("Scurvy Eugene", RenderObjects(ScurvyEugene));
        }

        [Test]
        public void RenderMostSpecificAliasIfAllAmbiguous()
        {
            var sam = new TestEntity(Pronoun.He, "Sam");
            var cloneOfSam = new TestEntity(Pronoun.He, "Sam");

            RenderObjects(sam);
            RenderObjects(cloneOfSam);
            Assert.AreEqual("Sam and Sam", RenderObjects(sam, cloneOfSam));
        }

        [Test]
        public void UsePronounAfterFirstRendering()
        {
            RenderObjects(ScurvyEugene);
            Assert.AreEqual("he", RenderObjects(ScurvyEugene));
        }

        [Test]
        public void UseSecondLeastSpecificAliasIfPronounUsed()
        {
            RenderObjects(ScurvyEugene, Paul);
            Assert.AreEqual("Scurvy", RenderObjects(ScurvyEugene));
        }

        [Test]
        public void SecondReferenceToMyGroupReferredToAsWe()
        {
            RenderObjects(Me, Ana);
            Assert.AreEqual("we", RenderObjects(Me, Ana));
            Assert.AreEqual("we", RenderObjects(Ana, Me));
        }

        [Test]
        public void SecondReferenceToOurGroupReferredToAsWe()
        {
            RenderObjects(Me, You);
            Assert.AreEqual("we", RenderObjects(Me, You));
            Assert.AreEqual("we", RenderObjects(You, Me));
        }

        [Test]
        public void SecondReferenceToGroupReferredToAsThey()
        {
            RenderObjects(Ana, Paul);
            Assert.AreEqual("they", RenderObjects(Ana, Paul));
            Assert.AreEqual("they", RenderObjects(Paul, Ana));
        }

        [Test]
        public void ReferToLargestGroupWithCollectiveAlias()
        {
            RenderObjects(Ana);
            RenderObjects(Ana, Me);
            Assert.AreEqual("we", RenderObjects(Me, Ana));
            Assert.AreEqual("we", RenderObjects(Ana, Me));
        }

        [Test]
        public void DoNotUseCollectivePronounForClauseContents()
        {
            var subClause1 = new They(Ana).Do("dance");
            var subClause2 = new They(Paul).Do("dance");

            RenderParagraph(new Them(subClause1, subClause2));
            Assert.AreEqual("She dances and he dances.", RenderParagraph(new Them(subClause1, subClause2)));
        }

        [Test]
        public void UseAliasesForObjectsMentionedEarlierInPhrases()
        {
            Assert.AreEqual(
                "Ana dances with Paul and John, and their cats.",
                RenderParagraph(new They(Ana).Do("dance") + " with " + new Them(Paul, John) + ", and " + new Their(Paul, John) + " cats"));
        }
    }
}
