﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using NUnit.Framework;
using Recurse.Spiel.English;
using Recurse.Spiel.English.Phrases.Elements;
using Recurse.Spiel.Test;
using Recurse.Spiel.Test.Helpers;
using System.Linq;

namespace Recurse.Spiel.Tests
{
    public class PronounOrderingTests : SpielTestFixture
    {
        [Test]
        public void RenderObjectPronounsInOrder()
        {
            var pronouns = new[] { Pronoun.He, Pronoun.You, Pronoun.Me, Pronoun.It, Pronoun.We, Pronoun.PluralThey };
            var objects = pronouns.Select(pronoun => (SpielArgument)new TestEntity(pronoun)).Concat(new SpielArgument[] { John }).ToArray();
            var expectedResult = "him, you, me, it, us, them, and John";

            Assert.AreEqual(expectedResult, RenderObjects(new Them(objects)));
            Assert.AreEqual(expectedResult, RenderObjects(new Them(objects.Reverse().ToArray())));
        }

        [Test]
        public void RenderSubjectPronounsInOrder()
        {
            var pronouns = new[] { Pronoun.He, Pronoun.You, Pronoun.Me, Pronoun.It, Pronoun.We, Pronoun.PluralThey };
            var objects = pronouns.Select(pronoun => (SpielArgument)new TestEntity(pronoun)).Concat(new SpielArgument[] { John }).ToArray();
            var expectedResult = "he, we, it, they, you, John, and I";

            Assert.AreEqual(expectedResult, RenderObjects(objects));
            Assert.AreEqual(expectedResult, RenderObjects(objects.Reverse().ToArray()));
        }
    }
}
