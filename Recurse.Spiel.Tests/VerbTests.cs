﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using NUnit.Framework;
using Recurse.Spiel.English;
using Recurse.Spiel.English.Phrases.Elements;
using Recurse.Spiel.Test;
using Recurse.Spiel.Test.Helpers;

namespace Recurse.Spiel.Tests
{
    public class VerbTests : SpielTestFixture
    {
        [Test]
        public void CorrectlyConjugatePronounVerbs()
        {
            Assert.AreEqual("I walk.", RenderParagraph(new They(Me).Do("walk")));
            Assert.AreEqual("You walk.", RenderParagraph(new They(You).Do("walk")));
            Assert.AreEqual("John walks.", RenderParagraph(new They(John).Do("walk")));
        }

        [Test]
        public void CorrectlyConjugatePresentTenseBe()
        {
            Assert.AreEqual("I am.", RenderParagraph(new They(Me).Are));
            Assert.AreEqual("We are.", RenderParagraph(new They(new TestEntity(Pronoun.We)).Are));
            Assert.AreEqual("You are.", RenderParagraph(new They(You).Are));
            Assert.AreEqual("John is.", RenderParagraph(new They(John).Are));
        }

        [Test]
        public void CorrectlyConjugateCollectionVerbs()
        {
            Assert.AreEqual("Ana and Paul are.", RenderParagraph(new They(Ana, Paul).Are));
        }
    }
}
