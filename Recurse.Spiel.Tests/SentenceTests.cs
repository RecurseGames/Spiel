﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using NUnit.Framework;
using Recurse.Spiel.Test;

namespace Recurse.Spiel.Tests
{
    public class SentenceTests : SpielTestFixture
    {
        [Test]
        public void RenderNonSentenceCase()
        {
            Assert.AreEqual("an apple", RenderObjects(AnApple));
        }

        [Test]
        public void RenderSentenceCase()
        {
            Assert.AreEqual("An apple.", RenderParagraph(AnApple));
        }

        [Test]
        public void RenderSentencePair()
        {
            Assert.AreEqual("An apple. John.", RenderParagraph(AnApple, John));
        }
    }
}
