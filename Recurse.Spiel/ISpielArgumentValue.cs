﻿namespace Recurse.Spiel
{
    /// <summary>
    /// Interface for an object which can be wrapped by a <see cref="SpielArgument"/>.
    /// </summary>
    public interface ISpielArgumentValue
    {
        /// <summary>
        /// <para>
        /// Returns a string rendering of this argument.
        /// </para>
        /// <para>
        /// If it's likely to be grammatically incorrect when included in a sentence,
        /// this suggests the rendering pipeline failed to handle it. In this case,
        /// the string should attempt to reflect what the argument actually is (e.g.,
        /// "he/him" for a pronoun with unknown declension).
        /// </para>
        /// <para>
        /// However, this method may also be used to display strings of arguments for
        /// debug purposes. Hence, for the sake of readability, the returned string
        /// should not be overly long.
        /// </para>
        /// </summary>
        string ToRenderString();
    }
}
