﻿namespace Recurse.Spiel
{
    /// <summary>
    /// Specifies whether a collection of <see cref="SpielArgument"/> instances
    /// represents a list of arguments (e.g., "A, B, C") or a sequence (e.g., "ABC").
    /// </summary>
    public enum SpielArgumentListType
    {
        /// <summary>
        /// Type for a collection of <see cref="SpielArgument"/> instances (e.g., "A, B, C")
        /// </summary>
        Collection,

        /// <summary>
        /// Type for a sequence of <see cref="SpielArgument"/> instances (e.g., "ABC")
        /// </summary>
        Sequence
    }
}
