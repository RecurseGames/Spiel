﻿namespace Recurse.Spiel
{
    public interface IArgumentTransform
    {
        SpielArgument TransformArgument(SpielArgument argument);
    }
}
