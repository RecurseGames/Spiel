﻿namespace Recurse.Spiel.English
{
    /// <summary>
    /// Interface for arguments which may have grammatical person (e.g.,
    /// first person). By default, elements are assumed to be third person.
    /// </summary>
    public interface IHasPerson
    {
        GrammaticalPerson GrammaticalPerson { get; }
    }
}
