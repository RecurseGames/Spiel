﻿namespace Recurse.Spiel.English
{
    /// <summary>
    /// Interface for arguments (e.g., pronouns) which require a specific
    /// ordering when rendered as part of a list
    /// </summary>
    public interface IOrdered : ISpielArgumentValue
    {
        int OrderKey { get; }
    }
}
