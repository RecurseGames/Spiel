﻿using System.Collections.Generic;
using System.Linq;

namespace Recurse.Spiel.English
{
    public static class Grammar
    {
        public static bool GetPlurality(SpielArgument argument)
        {
            if (argument.Value is INumbered numbered)
            {
                return numbered.IsPlural;
            }

            return false;
        }

        public static bool GetPlurality(IEnumerable<SpielArgument> arguments)
        {
            if (arguments == null)
            {
                return false;
            }

            var count = arguments.Count();
            if (count == 1)
            {
                return (arguments.Single().Value as INumbered)?.IsPlural == true;
            }
            else
            {
                return true;
            }
        }

        public static GrammaticalPerson GetPerson(IEnumerable<SpielArgument> arguments)
        {
            if (arguments == null)
            {
                return GrammaticalPerson.Third;
            }

            var persons = arguments
                .Select(a => a.Value)
                .OfType<IHasPerson>()
                .Select(e => e.GrammaticalPerson)
                .Distinct()
                .ToArray();
            if (persons.Contains(GrammaticalPerson.First))
            {
                return GrammaticalPerson.First;
            }
            else if (persons.Contains(GrammaticalPerson.Second))
            {
                return GrammaticalPerson.Second;
            }
            else
            {
                return GrammaticalPerson.Third;
            }
        }
    }
}
