﻿using Recurse.Spiel.Renderings;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Recurse.Spiel.English
{
    /// <summary>
    /// A rendering of a list of objects in possessive form (i.e., with apostrophe and S
    /// appended to them).
    /// </summary>
    [Serializable]
    public class PossessiveRendering : ISpielArgumentValue, IRendering
    {
        [DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
        private IEnumerable<SpielArgument> Elements { get; }

        public PossessiveRendering(IEnumerable<SpielArgument> elements)
        {
            Elements = elements;
        }

        public override string ToString()
        {
            var elements = string.Join(", ", Elements.Select(e => e.ToRenderedString()));

            return $"Possessive({elements})";
        }

        public string ToRenderString()
        {
            var elements = string.Join(", ", Elements.Select(e => e.ToRenderedString()));

            return elements.EndsWith("s", StringComparison.OrdinalIgnoreCase) ? $"{elements}'" : $"{elements}'s";
        }

        public IEnumerable<SpielArgument> GetRenderedElements()
        {
            yield return ListDelimiter.OxfordComma.Delimit(Elements);

            var lastElement = Elements.Last();
            var possessiveLastElement = lastElement.Value as IPossessive;
            if (possessiveLastElement == null || !possessiveLastElement.IsPossessive)
            {
                if (lastElement.ToString().EndsWith("s", StringComparison.OrdinalIgnoreCase))
                {
                    yield return "'";
                }
                else
                {
                    yield return "'s";
                }
            }
        }

        public static implicit operator SpielArgument(PossessiveRendering rendering)
        {
            return new SpielArgument(rendering);
        }
    }
}
