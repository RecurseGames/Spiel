﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Recurse.Spiel.English
{
    [Serializable]
    public class OrderingRenderStep : IArgumentsTransform
    {
        public IEnumerable<SpielArgument> TransformArguments(IReadOnlyCollection<SpielArgument> arguments)
        {
            return arguments.OrderBy(GetOrderKey);
        }

        private int GetOrderKey(SpielArgument argument)
        {
            return argument.Value is IOrdered ordered ? ordered.OrderKey : 0;
        }
    }
}
