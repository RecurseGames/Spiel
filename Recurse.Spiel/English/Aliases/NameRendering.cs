﻿using Recurse.Core.Collections;
using Recurse.Spiel.Aliases;
using System;
using System.Collections.Generic;

namespace Recurse.Spiel.English.Aliases
{
    [Serializable]
    public class NameRendering : IPossessive, IEquatable<NameRendering>, ISpielArgumentValue, IAliasRendering, INumbered, IOrdered, IHasPerson
    {
        private NameAlias NameAliasType { get; }

        public bool IsPossessive => NameAliasType.IsPossessive;

        public IAlias Alias => NameAliasType;

        public bool IsPlural => NameAliasType.IsPlural;

        public int OrderKey => NameAliasType.OrderKey;

        public GrammaticalPerson GrammaticalPerson => NameAliasType.GrammaticalPerson;

        public IReadOnlyCollection<SpielArgument> Referents { get; }

        public NameRendering(string name, GrammaticalPerson person, IReadOnlyCollection<SpielArgument> referents)
        {
            NameAliasType = new NameAlias(name) { GrammaticalPerson = person };
            Referents = referents;
        }

        public NameRendering(string name, IReadOnlyCollection<SpielArgument> referents)
        {
            NameAliasType = new NameAlias(name);
            Referents = referents;
        }

        public NameRendering(NameAlias type, IReadOnlyCollection<SpielArgument> referents)
        {
            NameAliasType = type;
            Referents = referents;
        }

        public static implicit operator SpielArgument(NameRendering rendering)
        {
            return new SpielArgument(rendering);
        }

        public bool Equals(NameRendering other)
        {
            return other != null
                && Equals(NameAliasType, other.NameAliasType)
                && MultisetComparer.Equals(Referents, other.Referents);
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as NameRendering);
        }

        public override int GetHashCode()
        {
            return NameAliasType.GetHashCode() + MultisetComparer.GetHashCode(Referents) + 0x2699dbf6;
        }

        public override string ToString()
        {
            return NameAliasType.Name;
        }

        public string ToRenderString()
        {
            return NameAliasType.Name;
        }
    }
}
