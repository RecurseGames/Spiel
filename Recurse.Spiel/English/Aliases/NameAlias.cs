﻿using Recurse.Spiel.Aliases;
using System;
using System.Collections.Generic;

namespace Recurse.Spiel.English.Aliases
{
    [Serializable]
    public class NameAlias : IAlias, IEquatable<NameAlias>
    {
        public string Name { get; }

        public bool IsPossessive { get; set; }

        public bool IsPlural { get; set; }

        public GrammaticalPerson GrammaticalPerson { get; set; }

        public int OrderKey { get; set; }

        public NameAlias(string name)
        {
            Name = name ?? throw new ArgumentNullException(nameof(name));
        }

        public IAliasRendering CreateAlias(params SpielArgument[] referents)
        {
            var nonEmptyReferents = SpielArgumentList.CreateNonEmpty(SpielArgumentListType.Collection, referents)
                ?? throw new ArgumentException("Must have at least 1 referent.", nameof(referents));
            return new NameRendering(this, nonEmptyReferents);
        }

        public IAliasRendering CreateAlias(IEnumerable<SpielArgument> referents)
        {
            var nonEmptyReferents = SpielArgumentList.CreateNonEmpty(SpielArgumentListType.Collection, referents)
                ?? throw new ArgumentException("Must have at least 1 referent.", nameof(referents));
            return new NameRendering(this, nonEmptyReferents);
        }

        public bool Equals(NameAlias other)
        {
            return other != null
                && Equals(Name, other.Name)
                && Equals(IsPlural, other.IsPlural)
                && Equals(GrammaticalPerson, other.GrammaticalPerson)
                && Equals(OrderKey, other.OrderKey)
                && Equals(IsPossessive, other.IsPossessive);
        }

        public bool Equals(IAlias other)
        {
            return Equals(other as NameAlias);
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as NameAlias);
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode() + 0x065d1ad7;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
