﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;
using Recurse.Core.Utils;
using Recurse.Spiel.Aliases;
using Recurse.Spiel.English.Aliases;

namespace Recurse.Spiel.English
{
    [Serializable]
    public class Pronoun : Key<int>, IAlias
    {
        /// <summary>
        /// Order key for standard pronouns (e.g., he/him, ze/zir, etc.).
        /// </summary>
        /// <remarks>
        /// -6: he / him
        /// -5: - / you
        /// -4: we / me
        /// -3: it / it
        /// -2: they / us
        /// -1: you / them
        /// +0: John / John
        /// +1: I / -
        /// </remarks>
        private const int StandardPronounOrderKey = -6;

        private static int NextId;

        public static readonly Pronoun
            It = new Pronoun("it", "it", "it", "its", "itself")
            {
                SubjectOrderKey = -3,
                DefaultOrderKey = -3
            },
            He = new Pronoun("he", "him", "his", "his", "himself"),
            She = new Pronoun("she", "her", "her", "hers", "herself"),
            SingularThey = new Pronoun("they", "them", "their", "theirs", "themself")
            {
                IsPlural = true,
                SubjectOrderKey = -2,
                DefaultOrderKey = -1
            },
            PluralThey = new Pronoun("they", "them", "their", "theirs", "themselves")
            {
                IsPlural = true,
                SubjectOrderKey = -2,
                DefaultOrderKey = -1
            },
            Spivak = new Pronoun("ey", "em", "eir", "eirs", "eirself"),
            Hir = new Pronoun("ze", "hir", "hir", "hirs", "hirself"),
            Zir = new Pronoun("ze", "zir", "zir", "zirs", "zirself"),
            Ne = new Pronoun("ne", "nem", "nir", "nirs", "nemself"), 
            Ve = new Pronoun("ve", "ver", "vis", "vis", "verself"),
            Xe = new Pronoun("xey", "xem", "xyr", "xyrs", "xemself"),
            //
            You = new Pronoun("you", "you", "your", "yours", "yourself")
            {
                GrammaticalPerson = GrammaticalPerson.Second,
                SubjectOrderKey = -1,
                DefaultOrderKey = -5,
            },
            We = new Pronoun("we", "us", "our", "ours", "ourselves")
            {
                GrammaticalPerson = GrammaticalPerson.Second,
                IsPlural = true,
                SubjectOrderKey = -4,
                DefaultOrderKey = -2,
            },
            Me = new Pronoun("I", "me", "my", "mine", "myself")
            {
                GrammaticalPerson = GrammaticalPerson.First,
                SubjectOrderKey = 1,
                DefaultOrderKey = -4,
            };

        public string They { get; }

        public string Them { get; }

        public string Their { get; }

        public string Theirs { get; }

        public string Themself { get; }

        public bool IsPlural { get; set; }

        public int SubjectOrderKey { get; set; } = StandardPronounOrderKey;

        public int DefaultOrderKey { get; set; } = StandardPronounOrderKey;

        public GrammaticalPerson GrammaticalPerson { get; set; } = GrammaticalPerson.Third;

        private Pronoun(
            string they,
            string them,
            string their,
            string theirs,
            string themself) : base(NextId++)
        {
            Them = them;
            They = they;
            Their = their;
            Theirs = theirs;
            Themself = themself;
        }

        public IAliasRendering CreateAlias(params SpielArgument[] referents)
        {
            var nonEmptyReferents = SpielArgumentList.CreateNonEmpty(SpielArgumentListType.Collection, referents)
                ?? throw new ArgumentException("Must have at least 1 referent.", nameof(referents));
            return new PronounAlias(this, nonEmptyReferents);
        }

        public bool Equals(IAlias other)
        {
            return Equals(other as Key<int>);
        }

        public override string ToString()
        {
            return They + "/" + Them;
        }
    }
}
