﻿using Recurse.Core.Collections;
using Recurse.Core.Exceptions;
using Recurse.Spiel.Aliases;
using Recurse.Spiel.English.Phrases;
using Recurse.Spiel.Renderings;
using System;
using System.Collections.Generic;

namespace Recurse.Spiel.English.Aliases
{
    [Serializable]
    public class PronounAlias : IPhraseElement, IEquatable<PronounAlias>, ISpielArgumentValue, IAliasRendering, IRendering
    {
        public IReadOnlyCollection<SpielArgument> Referents { get; }

        public Pronoun Pronoun { get; }

        public IAlias Alias => Pronoun;

        public PronounAlias(Pronoun pronoun, params SpielArgument[] referents)
        {
            Referents = SpielArgumentList.CreateNonEmpty(SpielArgumentListType.Collection, referents)
                ?? throw new ArgumentException("Must have at least 1 referent.", nameof(referents));
            Pronoun = pronoun;
        }

        public PronounAlias(Pronoun pronoun, IEnumerable<SpielArgument> referents)
        {
            Referents = SpielArgumentList.CreateNonEmpty(SpielArgumentListType.Collection, referents)
                ?? throw new ArgumentException("Must have at least 1 referent.", nameof(referents));
            Pronoun = pronoun;
        }

        public static implicit operator SpielArgument(PronounAlias alias)
        {
            return new SpielArgument(alias);
        }

        public SpielArgument Render(IPhraseElementContext context, ObjectDeclension declension)
        {
            return GetRendering(context.Subjects, declension);
        }

        public IEnumerable<SpielArgument> GetRenderedElements()
        {
            // Default declension is object (makes more sense to say "Me" instead of "I" out-of-context)
            yield return GetRendering(null, ObjectDeclension.Object);
        }

        public bool Equals(PronounAlias other)
        {
            return other != null
                && Equals(Pronoun, other.Pronoun)
                && MultisetComparer.Equals(Referents, other.Referents);
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as PronounAlias);
        }

        public override int GetHashCode()
        {
            return Pronoun.GetHashCode() + MultisetComparer.GetHashCode(Referents) + 0x516c0320;
        }

        public string ToRenderString()
        {
            return Pronoun.They;
        }

        public override string ToString()
        {
            return Pronoun.ToString();
        }

        private SpielArgument GetRendering(IEnumerable<SpielArgument> subjects, ObjectDeclension declension)
        {
            var pronounString = GetRenderingText(declension, subjects, Referents);
            var nameAliasType = new NameAlias(pronounString)
            {
                IsPlural = Pronoun.IsPlural,
                GrammaticalPerson = Pronoun.GrammaticalPerson,
                IsPossessive = true, // Note this indicates "'s" should not be appended to pronouns
                OrderKey = declension == ObjectDeclension.Subject ? Pronoun.SubjectOrderKey : Pronoun.DefaultOrderKey
            };
            return new SpielArgument(nameAliasType.CreateAlias(Referents));
        }

        private string GetRenderingText(ObjectDeclension declension, IEnumerable<SpielArgument> subjects, IEnumerable<SpielArgument> referents)
        {
            if (declension == ObjectDeclension.DependentPossessive)
            {
                return Pronoun.Their;
            }
            else if (declension == ObjectDeclension.IndependentPossessive)
            {
                return Pronoun.Theirs;
            }
            else if (declension == ObjectDeclension.Subject)
            {
                return Pronoun.They;
            }
            else if (declension == ObjectDeclension.Object)
            {
                if (MultisetComparer.Equals(subjects, referents))
                {
                    return Pronoun.Themself;
                }
                else
                {
                    return Pronoun.Them;
                }
            }
            else
            {
                throw new UnexpectedEnumException(declension);
            }
        }
    }
}
