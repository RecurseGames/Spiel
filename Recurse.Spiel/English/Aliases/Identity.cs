﻿using Recurse.Spiel.Aliases;
using Recurse.Spiel.References;
using System;

namespace Recurse.Spiel.English.Aliases
{
    /// <summary>
    /// An argument which refers to another argument by name.
    /// </summary>
    [Serializable]
    public class Identity : AliasIdentity, IAliased, ISpielArgumentValue
    {
        public Identity(SpielArgument referent, string firstName, string lastName) : base(referent)
        {
            if (firstName == null)
            {
                throw new ArgumentNullException(nameof(firstName));
            }
            else if (lastName == null)
            {
                throw new ArgumentNullException(nameof(lastName));
            }

            AliasFactories.Add(new FullNameAlias(firstName, lastName));
            AliasFactories.Add(new NameAlias(firstName));
        }

        public Identity(SpielArgument referent, string firstName) : this(referent)
        {
            if (firstName == null)
            {
                throw new ArgumentNullException(nameof(firstName));
            }

            AliasFactories.Add(new NameAlias(firstName));
        }

        public Identity(SpielArgument referent)
            : base(referent)
        {

        }
    }
}
