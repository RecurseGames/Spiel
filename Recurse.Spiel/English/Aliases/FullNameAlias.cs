﻿using Recurse.Core.Collections.Generic;
using Recurse.Core.Utils;
using Recurse.Spiel.Aliases;
using Recurse.Spiel.Renderings;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Recurse.Spiel.English.Aliases
{
    [Serializable]
    public class FullNameAlias : Equatable<IAlias>, IAlias
    {
        private string FirstName { get; }

        private string LastName { get; }

        protected override object Identity => ImmutableList.Create(FirstName, LastName);

        public FullNameAlias(string firstName, string lastName)
        {
            FirstName = firstName ?? throw new ArgumentNullException(nameof(firstName));
            LastName = lastName ?? throw new ArgumentNullException(nameof(lastName));
        }

        public IAliasRendering CreateAlias(params SpielArgument[] referents)
        {
            var nonEmptyReferents = SpielArgumentList.CreateNonEmpty(SpielArgumentListType.Collection, referents)
                ?? throw new ArgumentException("Must have at least 1 referent.", nameof(referents));
            return new Rendering(this, nonEmptyReferents);
        }

        public override string ToString()
        {
            return $"Alias({FirstName} {LastName})";
        }

        [Serializable]
        private class Collection : ISequenceMergable, INumbered, ISpielArgumentValue, IRendering
        {
            private string LastName => Aliases.First().LastName;

            protected FullNameAlias[] Aliases { get; }

            protected IReadOnlyCollection<SpielArgument>[] Referents { get; }

            public bool IsPlural => Aliases.Length > 1;

            public Collection(IEnumerable<FullNameAlias> aliases, IEnumerable<IReadOnlyCollection<SpielArgument>> referents)
            {
                Aliases = aliases.ToArray();
                Referents = referents.ToArray();
            }

            public Collection(FullNameAlias alias, IReadOnlyCollection<SpielArgument> referents)
            {
                Aliases = new[] { alias };
                Referents = new[] { referents };
            }

            public IEnumerable<SpielArgument> GetRenderedElements()
            {
                var elements = new List<ISpielArgumentValue>();
                for (var i = 0; i < Aliases.Length - 1; i++)
                {
                    elements.Add(new NameRendering(Aliases[i].FirstName, Referents[i]));
                }

                var lastIndex = Aliases.Length - 1;
                elements.Add(new NameRendering($"{Aliases[lastIndex].FirstName} {LastName}", Referents[lastIndex]));

                yield return ListDelimiter.OxfordComma.Delimit(elements);
            }

            public ISequenceMergable GetMerged(ISequenceMergable next)
            {
                if (next is Collection other && Equals(LastName, other.LastName))
                {
                    return new Collection(Aliases.Concat(other.Aliases), Referents.Concat(other.Referents));
                }
                else
                {
                    return null;
                }
            }

            public string ToRenderString()
            {
                var firstNames = ListDelimiter.OxfordComma.Delimit(Aliases.Select(e => e.FirstName)).ToRenderedString();

                return $"{firstNames} {LastName}";
            }
        }

        [Serializable]
        private class Rendering : Collection, IAliasRendering
        {
            IAlias IAliasRendering.Alias => Aliases.Single();

            IReadOnlyCollection<SpielArgument> IAliasRendering.Referents => Referents.Single();

            public Rendering(FullNameAlias alias, IReadOnlyCollection<SpielArgument> referents) : base(alias, referents)
            {

            }
        }
    }
}
