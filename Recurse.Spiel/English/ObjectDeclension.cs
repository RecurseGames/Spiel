﻿namespace Recurse.Spiel.English
{
    public enum ObjectDeclension
    {
        Subject,

        Object,

        DependentPossessive,

        IndependentPossessive
    }
}
