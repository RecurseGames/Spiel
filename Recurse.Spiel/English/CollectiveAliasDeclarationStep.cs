﻿using Recurse.Spiel.Aliases;
using Recurse.Spiel.English.Aliases;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Recurse.Spiel.English
{
    public class CollectiveAliasDeclarationStep : IArgumentsTransform
    {
        private readonly AliasContext _transientPronouns;

        private readonly AliasContext _fixedAliases;

        private readonly IEnumerable<SpielArgument> _sourceArguments;

        public CollectiveAliasDeclarationStep(
            IEnumerable<SpielArgument> sourceArguments,
            AliasContext transientPronouns,
            AliasContext fixedAliases)
        {
            _sourceArguments = sourceArguments ?? throw new ArgumentNullException(nameof(sourceArguments));
            _transientPronouns = transientPronouns ?? throw new ArgumentNullException(nameof(transientPronouns));
            _fixedAliases = fixedAliases ?? throw new ArgumentNullException(nameof(fixedAliases));
        }

        public IEnumerable<SpielArgument> TransformArguments(IReadOnlyCollection<SpielArgument> arguments)
        {
            // The current design of spiel makes it almost inevitable that plural "you" will be ambiguous.
            // As a result, we do not use it.

            if (arguments == null)
            {
                throw new ArgumentNullException(nameof(arguments));
            }

            // Implicitly, anything with a valid object declension may be referred to
            // by a collective pronoun. If we want have non-object lists (e.g., "I saw [you danced, he danced]")
            // we need to use the Clause(...) element. If we want to mix objects and non-objects,
            // we'll have to figure it out ourselves with something like They(Jon) + Are + List(Them(Jon), Ugly)

            if (_sourceArguments.Count() > 1)
            {
                var firstPersonAlias = _fixedAliases.GetUsableAlias(Pronoun.Me);
                var secondPersonAlias = _fixedAliases.GetUsableAlias(Pronoun.You);
                var firstPersonIncluded = firstPersonAlias?.Referents.All(_sourceArguments.Contains) == true;
                var secondPersonIncluded = secondPersonAlias?.Referents.All(_sourceArguments.Contains) == true;
                if (firstPersonIncluded)
                {
                    var alias = new PronounAlias(Pronoun.We, _sourceArguments);

                    _transientPronouns.Add(alias);
                }
                else if (!secondPersonIncluded)
                {
                    var alias = new PronounAlias(Pronoun.PluralThey, _sourceArguments);

                    _transientPronouns.Add(alias);
                }
            }
            return arguments;
        }
    }
}
