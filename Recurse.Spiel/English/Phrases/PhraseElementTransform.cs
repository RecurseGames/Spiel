﻿using System;

namespace Recurse.Spiel.English.Phrases
{
    [Serializable]
    public class PhraseElementTransform : IArgumentTransform
    {
        private readonly IPhraseElementContext _context;

        private readonly ObjectDeclension _declension;

        public PhraseElementTransform(IPhraseElementContext context, ObjectDeclension declension)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _declension = declension;
        }

        public SpielArgument TransformArgument(SpielArgument argument)
        {
            if (argument.Value is IPhraseElement element)
            {
                return element.Render(_context, _declension);
            }
            else
            {
                return argument;
            }
        }
    }
}
