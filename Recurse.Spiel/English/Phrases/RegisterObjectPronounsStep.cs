﻿using System;

namespace Recurse.Spiel.Aliases
{
    /// <summary>
    /// Renderer which ensures pronouns of any objects are recorded.
    /// </summary>
    [Serializable]
    public class RegisterObjectPronounsStep : IArgumentTransform
    {
        private readonly AliasContext _context;

        public RegisterObjectPronounsStep(AliasContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public SpielArgument TransformArgument(SpielArgument argument)
        {
            if (argument.Value is IObject obj)
            {
                _context.Add(obj.Pronouns.CreateAlias(new SpielArgument(obj)));
            }
            
            return argument;
        }
    }
}
