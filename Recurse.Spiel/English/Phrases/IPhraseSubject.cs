﻿using System.Collections.Generic;

namespace Recurse.Spiel.English.Phrases
{
    /// <summary>
    /// Interface for a phrase's subject element.
    /// </summary>
    public interface IPhraseSubject
    {
        IEnumerable<SpielArgument> Elements { get; }
    }
}
