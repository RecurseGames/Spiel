﻿using System;
using System.Collections.Generic;

namespace Recurse.Spiel.English.Phrases
{
    public class ObjectContextChild : IPhraseElementContext
    {
        private readonly IPhraseElementContext _parent;

        private IEnumerable<SpielArgument> _subjects;

        private IEnumerable<SpielArgument> _renderedSubjects;

        public IEnumerable<SpielArgument> Subjects => _subjects ?? _parent.Subjects;

        public IEnumerable<SpielArgument> RenderedSubjects => _renderedSubjects ?? _parent.RenderedSubjects;

        public ObjectContextChild(IPhraseElementContext parent)
        {
            _parent = parent ?? throw new ArgumentNullException(nameof(parent));
        }

        public void AddSubject(IEnumerable<SpielArgument> subjects, IEnumerable<SpielArgument> renderedSubjects)
        {
            if (subjects == null)
            {
                throw new ArgumentNullException(nameof(subjects));
            }
            else if (renderedSubjects == null)
            {
                throw new ArgumentNullException(nameof(renderedSubjects));
            }
            else if (_subjects != null)
            {
                throw new InvalidOperationException("Subject already added.");
            }

            _subjects = subjects;
            _renderedSubjects = renderedSubjects;
        }
    }
}
