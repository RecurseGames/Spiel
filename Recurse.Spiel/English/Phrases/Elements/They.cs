﻿using Recurse.Core.Exceptions;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Recurse.Spiel.English.Phrases.Elements
{
    [Serializable]
    public class They : NounPhrase, IPhraseSubject
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public SpielSubstring Were => this + Verbs.Were;

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public SpielSubstring Are => this + Verbs.Are;

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public SpielSubstring Arent => this + Verbs.Arent;

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public SpielSubstring Does => this + Verbs.Does;

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public SpielSubstring Doesnt => this + Verbs.Doesnt;

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public SpielSubstring Have => this + Verbs.Have;

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public SpielSubstring Havent => this + Verbs.Havent;

        public override bool IsRoot => true;

        public They(params SpielArgument[] objects) : base(ObjectDeclension.Subject, objects)
        {

        }

        public They(IEnumerable<SpielArgument> objects) : base(ObjectDeclension.Subject, objects)
        {

        }

        public They(IEnumerable<ISpielArgumentValue> objects) : base(ObjectDeclension.Subject, objects)
        {

        }

        public SpielSubstring Is(Tense tense)
        {
            switch (tense)
            {
                case Tense.Present:
                    return Are;
                case Tense.Past:
                    return Were;
                default:
                    throw new UnexpectedEnumException(tense);
            }
        }

        public SpielSubstring Can(Tense tense)
        {
            switch (tense)
            {
                case Tense.Present:
                    return this + new Verb("can");
                case Tense.Past:
                    return this + new Verb("could");
                default:
                    throw new UnexpectedEnumException(tense);
            }
        }

        public SpielSubstring Is(bool value) => value ? Are : Arent;

        public SpielSubstring AreOrWere(bool are) => are ? Are : Were;

        public SpielSubstring Has(bool value) => value ? Have : Havent;

        public SpielSubstring Do(string verb) => this + new Verb(verb);

        public SpielSubstring Did(string verb) => this + new Verb(verb);
    }
}
