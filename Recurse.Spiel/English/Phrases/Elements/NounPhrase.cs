﻿using Recurse.Core.Collections;
using Recurse.Spiel.Renderings;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Recurse.Spiel.English.Phrases.Elements
{
    /// <summary>
    /// A phrase element which is a list of objects
    /// </summary>
    [Serializable]
    public abstract class NounPhrase : SpielStringElement, IEquatable<NounPhrase>, IPhraseElementComposite
    {
        public ObjectDeclension Declension { get; }

        [DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
        public IEnumerable<SpielArgument> Elements { get; }

        protected NounPhrase(ObjectDeclension declension, IEnumerable<SpielArgument> elements)
        {
            Declension = declension;
            Elements = SpielArgumentList.CreateNonEmpty(SpielArgumentListType.Collection, elements) 
                ?? throw new ArgumentException("At least 1 object required.", nameof(elements));
        }

        protected NounPhrase(ObjectDeclension declension, IEnumerable<ISpielArgumentValue> elements)
        {
            Declension = declension;
            Elements = SpielArgumentList.CreateNonEmpty(SpielArgumentListType.Collection, elements.Select(e => new SpielArgument(e)))
                ?? throw new ArgumentException("At least 1 object required.", nameof(elements));
        }

        public override string ToString()
        {
            return GetType().Name + "(" + string.Join(", ", Elements.Select(e => e.ToRenderedString())) + ")";
        }

        public override string ToRenderString()
        {
            if (Declension == ObjectDeclension.DependentPossessive || Declension == ObjectDeclension.IndependentPossessive)
            {
                return new PossessiveRendering(Elements).ToRenderString();
            }
            else
            {
                return ListDelimiter.OxfordComma.Delimit(Elements).ToRenderedString();
            }
        }

        public SpielArgument Render(IPhraseRenderer context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            var renderedObjects = context.RenderObjects(Declension, Elements);
            if (Declension == ObjectDeclension.DependentPossessive || Declension == ObjectDeclension.IndependentPossessive)
            {
                return new PossessiveRendering(renderedObjects);
            }
            else
            {
                return ListDelimiter.OxfordComma.Delimit(renderedObjects);
            }
        }

        public bool Equals(NounPhrase other)
        {
            return other != null
                && MultisetComparer.Equals(Elements, other.Elements)
                && Equals(Declension, other.Declension);
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as NounPhrase);
        }

        public override int GetHashCode()
        {
            return MultisetComparer.GetHashCode(Elements) + Declension.GetHashCode() + 0x6b2cbf67;
        }
    }
}
