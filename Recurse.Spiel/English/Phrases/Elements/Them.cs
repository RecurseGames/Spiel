﻿using System;
using System.Collections.Generic;

namespace Recurse.Spiel.English.Phrases.Elements
{
    [Serializable]
    public class Them : NounPhrase
    {
        public override bool IsRoot => false;

        public Them(params SpielArgument[] objects) : base(ObjectDeclension.Object, objects)
        {

        }

        public Them(IEnumerable<SpielArgument> objects) : base(ObjectDeclension.Object, objects)
        {

        }

        public Them(IEnumerable<ISpielArgumentValue> objects) : base(ObjectDeclension.Object, objects)
        {

        }
    }
}
