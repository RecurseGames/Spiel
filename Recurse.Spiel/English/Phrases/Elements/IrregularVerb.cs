﻿using System;

namespace Recurse.Spiel.English.Phrases.Elements
{
    [Serializable]
    public class IrregularVerb : SpielStringElement, IEquatable<IrregularVerb>, IPhraseElement
    {
        private string FirstPersonSingular { get; }

        private string SecondPersonSingular { get; }

        private string ThirdPersonSingular { get; }

        private string FirstPersonPlural { get; }

        private string SecondPersonPlural { get; }

        private string ThirdPersonPlural { get; }

        public override bool IsRoot => false;

        public IrregularVerb(
            string firstPersonSingular, string secondPersonSingular, string thirdPersonSingular,
            string firstPersonPlural, string secondPersonPlural, string thirdPersonPlural)
        {
            FirstPersonSingular = firstPersonSingular ?? throw new ArgumentNullException(nameof(firstPersonSingular));
            SecondPersonSingular = secondPersonSingular ?? throw new ArgumentNullException(nameof(secondPersonSingular));
            ThirdPersonSingular = thirdPersonSingular ?? throw new ArgumentNullException(nameof(thirdPersonSingular));
            FirstPersonPlural = firstPersonPlural ?? throw new ArgumentNullException(nameof(firstPersonPlural));
            SecondPersonPlural = secondPersonPlural ?? throw new ArgumentNullException(nameof(secondPersonPlural));
            ThirdPersonPlural = thirdPersonPlural ?? throw new ArgumentNullException(nameof(thirdPersonPlural));
        }

        public SpielArgument Render(IPhraseElementContext context, ObjectDeclension declension)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            var isPlural = Grammar.GetPlurality(context.RenderedSubjects);
            var person = Grammar.GetPerson(context.RenderedSubjects);

            switch (person)
            {
                case GrammaticalPerson.First:
                    return isPlural ? FirstPersonPlural : FirstPersonSingular;
                case GrammaticalPerson.Second:
                    return isPlural ? SecondPersonPlural : SecondPersonSingular;
                default:
                    return isPlural ? ThirdPersonPlural : ThirdPersonSingular;
            }
        }

        public override string ToString()
        {
            // Without context, we assume imperative form ("DO x")
            return $"Verb({ThirdPersonPlural})";
        }

        public override string ToRenderString()
        {
            return $"{ThirdPersonSingular}/{ThirdPersonPlural}";
        }

        public bool Equals(IrregularVerb other)
        {
            return other != null
                && Equals(FirstPersonSingular, other.FirstPersonSingular)
                && Equals(SecondPersonSingular, other.SecondPersonSingular)
                && Equals(ThirdPersonSingular, other.ThirdPersonSingular)
                && Equals(FirstPersonPlural, other.FirstPersonPlural)
                && Equals(SecondPersonPlural, other.SecondPersonPlural)
                && Equals(ThirdPersonPlural, other.ThirdPersonPlural);
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as IrregularVerb);
        }

        public override int GetHashCode()
        {
            return ThirdPersonPlural.GetHashCode();
        }
    }
}
