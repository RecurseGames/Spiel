﻿using Recurse.Core.Extensions.String;
using System;

namespace Recurse.Spiel.English.Phrases.Elements
{
    [Serializable]
    public class Verb : SpielStringElement, IEquatable<Verb>, IPhraseElement
    {
        private string Infinitive { get; }

        private string ThirdPersonSingular { get; }

        public override bool IsRoot => false;

        public Verb(string infinitive, string thirdPersonSingular)
        {
            Infinitive = infinitive ?? throw new ArgumentNullException(nameof(infinitive));
            ThirdPersonSingular = thirdPersonSingular ?? throw new ArgumentNullException(nameof(thirdPersonSingular));
        }

        public Verb(string infinitive)
        {
            Infinitive = infinitive ?? throw new ArgumentNullException(nameof(infinitive));
            ThirdPersonSingular = StringUtils.getRegularPlural(infinitive);
        }

        public SpielArgument Render(IPhraseElementContext context, ObjectDeclension declension)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            if (Grammar.GetPerson(context.RenderedSubjects) == GrammaticalPerson.Third
                && !Grammar.GetPlurality(context.RenderedSubjects))
            {
                return ThirdPersonSingular;
            }
            else
            {
                return Infinitive;
            }
        }

        public bool Equals(Verb other)
        {
            return other != null
                && Equals(Infinitive, other.Infinitive)
                && Equals(ThirdPersonSingular, other.ThirdPersonSingular);
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as Verb);
        }

        public override int GetHashCode()
        {
            return ThirdPersonSingular.GetHashCode();
        }

        public override string ToString()
        {
            // Without context, we assume imperative form ("DO x")
            return Infinitive;
        }

        public override string ToRenderString()
        {
            return $"{ThirdPersonSingular}/{Infinitive}";
        }
    }
}
