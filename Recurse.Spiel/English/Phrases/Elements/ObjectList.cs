﻿using Recurse.Spiel.Groupings;
using Recurse.Spiel.Renderers;
using Recurse.Spiel.Renderings;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Recurse.Spiel.English.Phrases.Elements
{
    /// <summary>
    /// A list of objects which are not replaced with pronouns.
    /// </summary>
    [Serializable]
    public class ObjectList : SpielStringElement, IClauseContainer
    {
        public override bool IsRoot => false;

        private IEnumerable<SpielArgument> Elements { get; }

        public ObjectList(IEnumerable<SpielArgument> elements)
        {
            Elements = SpielArgumentList.CreateNonEmpty(SpielArgumentListType.Collection, elements)
                ?? throw new ArgumentException("At least 1 object required.", nameof(elements));
        }

        public ObjectList(IEnumerable<ISpielArgumentValue> elements)
        {
            Elements = SpielArgumentList.CreateNonEmpty(SpielArgumentListType.Collection, elements.Select(e => new SpielArgument(e)))
                ?? throw new ArgumentException("At least 1 object required.", nameof(elements));
        }

        public ObjectList(params SpielArgument[] elements)
        {
            Elements = SpielArgumentList.CreateNonEmpty(SpielArgumentListType.Collection, elements)
                ?? throw new ArgumentException("At least 1 object required.", nameof(elements));
        }

        public SpielArgument Render(ISentenceRenderer renderer)
        {
#warning This should use a special declension which forbids plural they, he/him, etc... but allows You
            var renderedElements = renderer.RenderSentences(Elements);

            return ListDelimiter.Comma.Delimit(renderedElements);
        }

        public override string ToString()
        {
            return GetType().Name + "(" + string.Join(", ", Elements) + ")";
        }

        public override string ToRenderString()
        {
            return ListDelimiter.Comma.Delimit(Elements).ToRenderedString();
        }
    }
}
