﻿using System;
using System.Collections.Generic;

namespace Recurse.Spiel.English.Phrases.Elements
{
    [Serializable]
    public class Theirs : NounPhrase
    {
        public override bool IsRoot => false;

        public Theirs(params SpielArgument[] objects) : base(ObjectDeclension.IndependentPossessive, objects)
        {

        }

        public Theirs(IEnumerable<SpielArgument> objects) : base(ObjectDeclension.IndependentPossessive, objects)
        {

        }

        public Theirs(IEnumerable<ISpielArgumentValue> objects) : base(ObjectDeclension.IndependentPossessive, objects)
        {

        }
    }
}
