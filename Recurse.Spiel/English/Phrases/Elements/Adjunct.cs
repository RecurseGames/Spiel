﻿using Recurse.Core.Collections;
using Recurse.Spiel.Renderings;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Recurse.Spiel.English.Phrases.Elements
{
    [Serializable]
    public class Adjunct : SpielStringElement, ISequenceMergable, IPhraseElementComposite
    {
        private SpielArgumentList Prefixes { get; }

        private SpielArgumentList Elements { get; }

        public override bool IsRoot => false;

        public Adjunct(SpielArgument prefix, params SpielArgument[] elements)
        {
            Prefixes = SpielArgumentList.CreateNonEmpty(SpielArgumentListType.Collection, prefix)
                ?? throw new ArgumentException("At least 1 prefix required.", nameof(prefix));
            Elements = SpielArgumentList.CreateNonEmpty(SpielArgumentListType.Collection, elements)
                ?? throw new ArgumentException("At least 1 object required.", nameof(elements));
        }

        private Adjunct(IEnumerable<SpielArgument> prefixes, IEnumerable<SpielArgument> elements)
        {
            Prefixes = SpielArgumentList.CreateNonEmpty(SpielArgumentListType.Collection, prefixes)
                ?? throw new ArgumentException("At least 1 prefix required.", nameof(prefixes));
            Elements = SpielArgumentList.CreateNonEmpty(SpielArgumentListType.Collection, elements)
                ?? throw new ArgumentException("At least 1 object required.", nameof(elements));
        }

        public ISequenceMergable GetMerged(ISequenceMergable other)
        {
            var otherAdjunct = other as Adjunct;
            if (otherAdjunct != null)
            {
                if (MultisetComparer.Equals(Prefixes, otherAdjunct.Prefixes))
                {
                    return new Adjunct(Prefixes, Elements.Concat(otherAdjunct.Elements).ToArray());
                }
                else if (MultisetComparer.Equals(Elements, otherAdjunct.Elements))
                {
                    return new Adjunct(Prefixes.Concat(otherAdjunct.Prefixes).ToArray(), Elements);
                }
            }
            return null;
        }

        public SpielArgument Render(IPhraseRenderer context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            var renderedElements = new List<SpielArgument>();
            var renderedPrefixes = context.RenderPhrase(Prefixes);
            renderedElements.Add(ListDelimiter.OxfordComma.Delimit(renderedPrefixes));
            renderedElements.Add(" ");

            var renderedObjects = context.RenderObjects(ObjectDeclension.Object, Elements);
            renderedElements.Add(ListDelimiter.OxfordComma.Delimit(renderedObjects));

            return new CompositeRendering(renderedElements);
        }

        public override string ToString()
        {
            var prefixes = ListDelimiter.OxfordComma.Delimit(Prefixes.Select<SpielArgument, SpielArgument>(e => e.ToRenderedString())).ToRenderedString();
            var elements = ListDelimiter.OxfordComma.Delimit(Elements.Select<SpielArgument, SpielArgument>(e => e.ToRenderedString())).ToRenderedString();
            return $"{GetType().Name}({prefixes}, {elements})";
        }

        public override string ToRenderString()
        {
            var prefixes = ListDelimiter.OxfordComma.Delimit(Prefixes.Select<SpielArgument, SpielArgument>(e => e.ToRenderedString())).ToRenderedString();
            var elements = ListDelimiter.OxfordComma.Delimit(Elements.Select<SpielArgument, SpielArgument>(e => e.ToRenderedString())).ToRenderedString();
            return $"{prefixes} {elements}";
        }
    }
}
