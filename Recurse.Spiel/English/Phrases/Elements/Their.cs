﻿using System;
using System.Collections.Generic;

namespace Recurse.Spiel.English.Phrases.Elements
{
    [Serializable]
    public class Their : NounPhrase
    {
        public override bool IsRoot => false;

        public Their(params SpielArgument[] objects) : base(ObjectDeclension.DependentPossessive, objects)
        {

        }

        public Their(IEnumerable<SpielArgument> objects) : base(ObjectDeclension.DependentPossessive, objects)
        {

        }

        public Their(IEnumerable<ISpielArgumentValue> objects) : base(ObjectDeclension.DependentPossessive, objects)
        {

        }
    }
}
