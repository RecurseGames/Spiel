﻿using System;

namespace Recurse.Spiel.English.Phrases.Elements
{
    [Serializable]
    public static class Verbs
    {
        public static SpielStringElement Were { get; } = new IrregularVerb("was", "were", "was", "were", "were", "were");

        public static SpielStringElement Are { get; } = new IrregularVerb("am", "are", "is", "are", "are", "are");

        public static SpielStringElement Is(Tense tense) => tense == Tense.Past ? Were : Are;

        public static SpielStringElement Arent { get; } = new IrregularVerb("am not", "aren't", "isn't", "aren't", "aren't", "aren't");

        public static SpielStringElement Does { get; } = new Verb("do", "does");

        public static SpielStringElement Doesnt { get; } = new Verb("don't", "doesn't");

        public static SpielStringElement Have { get; } = new Verb("have", "has");

        public static SpielStringElement Havent { get; } = new Verb("haven't", "hasn't");

        public static SpielStringElement Can { get; } = new Verb("can");

        public static SpielStringElement Cant { get; } = new Verb("can't");

        public static SpielStringElement GetAreCurrently(bool areCurrently) => areCurrently ? Are : Were;

        public static SpielStringElement GetCan(bool can) => can ? Can : Cant;
    }
}
