﻿namespace Recurse.Spiel.English.Phrases.Elements
{
    public enum Tense
    {
        Present, Past
    }
}
