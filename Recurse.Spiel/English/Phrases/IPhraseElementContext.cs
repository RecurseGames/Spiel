﻿using System.Collections.Generic;

namespace Recurse.Spiel.English.Phrases
{
    public interface IPhraseElementContext
    {
        /// <summary>
        /// The subjects of the phrase, or null if it has no subject.
        /// </summary>
        IEnumerable<SpielArgument> Subjects { get; }

        /// <summary>
        /// The rendered subjects of the phrase, or null if it has no subject.
        /// </summary>
        IEnumerable<SpielArgument> RenderedSubjects { get; }
    }
}
