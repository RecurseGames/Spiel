﻿using System.Collections.Generic;

namespace Recurse.Spiel.English.Phrases
{
    public interface IPhraseRenderer
    {
        IEnumerable<SpielArgument> RenderPhrase(SpielArgumentList elements);

        IEnumerable<SpielArgument> RenderObjects(ObjectDeclension declension, IEnumerable<SpielArgument> objects);
    }
}
