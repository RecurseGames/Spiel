﻿namespace Recurse.Spiel.Aliases
{
    public interface IObject : ISpielArgumentValue
    {
        IAlias Pronouns { get; }
    }
}
