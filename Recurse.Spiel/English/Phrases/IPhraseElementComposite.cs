﻿namespace Recurse.Spiel.English.Phrases
{
    /// <summary>
    /// A phrase (or part of one) which is part of another phrase.
    /// This could be a list, like [A, B, and C], or some other construct like
    /// [A, B, or C] or [with A].
    /// </summary>
    public interface IPhraseElementComposite : ISpielArgumentValue
    {
        SpielArgument Render(IPhraseRenderer renderer);
    }
}
