﻿using Recurse.Spiel.Renderings;
using System;
using System.Collections.Generic;

namespace Recurse.Spiel.English.Phrases
{
    public class RenderSubjectStep : IArgumentsTransform
    {
        private IPhraseRenderer _renderer;

        public ObjectContextChild _objectContext;

        public IPhraseElementContext ObjectContext => _objectContext;

        public RenderSubjectStep(IPhraseRenderer renderer, IPhraseElementContext context)
        {
            _renderer = renderer ?? throw new ArgumentNullException(nameof(renderer));
            _objectContext = new ObjectContextChild(context ?? throw new ArgumentNullException(nameof(context)));
        }

        public IEnumerable<SpielArgument> TransformArguments(IReadOnlyCollection<SpielArgument> arguments)
        {
            var result = new List<SpielArgument>();
            foreach (var argument in arguments)
            {
                if (argument.Value is IPhraseSubject subjectArgument)
                {
                    var subjects = SpielArgumentList.Create(SpielArgumentListType.Collection, subjectArgument.Elements);
                    var renderedSubjects = _renderer.RenderObjects(ObjectDeclension.Subject, subjects);
                    var renderedSubject = ListDelimiter.OxfordComma.Delimit(renderedSubjects);

                    _objectContext.AddSubject(subjects, renderedSubjects);

                    result.Add(renderedSubject);
                }
                else
                {
                    result.Add(argument);
                }
            }
            return result;
        }
    }
}
