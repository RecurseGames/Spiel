﻿using System.Collections.Generic;

namespace Recurse.Spiel.English.Phrases
{
    public class ObjectContextRoot : IPhraseElementContext
    {
        public static ObjectContextRoot Instance { get; } = new ObjectContextRoot();

        public IEnumerable<SpielArgument> Subjects { get; }

        public IEnumerable<SpielArgument> RenderedSubjects { get; }

        private ObjectContextRoot()
        {

        }
    }
}
