﻿using System;

namespace Recurse.Spiel.English.Phrases
{
    [Serializable]
    public class PhraseElementCompositeTransform : IArgumentTransform
    {
        private readonly IPhraseRenderer _context;

        public PhraseElementCompositeTransform(IPhraseRenderer context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public SpielArgument TransformArgument(SpielArgument argument)
        {
            if (argument.Value is IPhraseElementComposite parent)
            {
                return parent.Render(_context);
            }
            else
            {
                return argument;
            }
        }
    }
}
