﻿namespace Recurse.Spiel.English.Phrases
{
    /// <summary>
    /// Interface for arguments which may be displayed as part
    /// of a phrase (such as the subject or object of a sentence).
    /// </summary>
    /// <remarks>
    /// Phrases may recursively contain other phrases.
    /// </remarks>
    public interface IPhraseElement : ISpielArgumentValue
    {
        SpielArgument Render(IPhraseElementContext context, ObjectDeclension declension);
    }
}
