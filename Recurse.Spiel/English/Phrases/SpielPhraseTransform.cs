﻿using Recurse.Spiel.Renderings;
using System;

namespace Recurse.Spiel.English.Phrases
{
    [Serializable]
    public class SpielPhraseTransform : IArgumentTransform
    {
        private readonly IPhraseRenderer _renderer;

        public SpielPhraseTransform(IPhraseRenderer renderer)
        {
            _renderer = renderer ?? throw new ArgumentNullException(nameof(renderer));
        }

        public SpielArgument TransformArgument(SpielArgument argument)
        {
            if (argument.Value is SpielString phrase)
            {
                var elements = SpielArgumentList.Create(SpielArgumentListType.Sequence, phrase.Elements);
                var renderedElements = _renderer.RenderPhrase(elements);

                return new CompositeRendering(renderedElements);
            }
            else
            {
                return argument;
            }
        }
    }
}
