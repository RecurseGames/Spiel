﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Spiel.English.Phrases;
using Recurse.Spiel.English.Phrases.Elements;
using Recurse.Spiel.Renderings;
using System;
using System.Linq;

namespace Recurse.Spiel.English
{
    [Serializable]
    public class A : SpielStringElement, ISpielArgumentValue, INumbered
    {
        private readonly SpielArgument _argument;

        public bool IsPlural => Grammar.GetPlurality(_argument);

        public override bool IsRoot => false;

        public A(SpielArgument argument)
        {
            _argument = argument;
        }

        public A(ISpielArgumentValue argument)
        {
            _argument = new SpielArgument(argument);
        }

        public override string ToString()
        {
            return $"{GetType().Name}({_argument})";
        }

        public override string ToRenderString()
        {
            var renderedArguments = _argument.ToRenderedString();
            var prefix = GetPrefix(renderedArguments);
            return $"{prefix}{renderedArguments}";
        }

        public SpielArgument Render(IPhraseRenderer renderer)
        {
            var arguments = SpielArgumentList.Create(SpielArgumentListType.Collection, _argument);
            var renderedArgumentElements = renderer.RenderPhrase(arguments);
            var prefix = GetPrefix(renderedArgumentElements.First().ToRenderedString());
            return new PrefixedRendering(prefix, renderedArgumentElements);
        }

        public static implicit operator SpielArgument(A a)
        {
            return new SpielArgument(a);
        }

        private string GetPrefix(string renderedArguments)
        {
            var firstRenderedArgumentWord = renderedArguments.Split(new[] { ' ' }, 2)[0];
            if (firstRenderedArgumentWord.StartsWith("8")
                || firstRenderedArgumentWord.StartsWith("a")
                || firstRenderedArgumentWord.StartsWith("e")
                || firstRenderedArgumentWord.StartsWith("i")
                || firstRenderedArgumentWord.StartsWith("o")
                || firstRenderedArgumentWord.StartsWith("u")
                || firstRenderedArgumentWord.Equals("11")
                || firstRenderedArgumentWord.Equals("18"))
            {
                return "an ";
            }
            else
            {
                return "a ";
            }
        }

        public class Renderer : IArgumentTransform
        {
            private readonly IPhraseRenderer _renderer;

            public Renderer(IPhraseRenderer renderer)
            {
                _renderer = renderer;
            }

            public SpielArgument TransformArgument(SpielArgument argument)
            {
                if (argument.Value is A a)
                {
                    return a.Render(_renderer);
                }
                else
                {
                    return argument;
                }
            }
        }
    }
}
