﻿namespace Recurse.Spiel.English
{
    /// <summary>
    /// Interface for arguments which may be in possessive form,
    /// and thus not require apostrophes or S to be appended to them
    /// in possessive lists
    /// </summary>
    public interface IPossessive : ISpielArgumentValue
    {
        bool IsPossessive { get; }
    }
}
