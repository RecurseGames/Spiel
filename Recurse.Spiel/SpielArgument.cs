﻿using System;
using System.Diagnostics;

namespace Recurse.Spiel
{
    /// <summary>
    /// <para>
    /// Wrapper for an object which can be, or has been, rendered by Spiel. These can be strings,
    /// but also more complex objects representing concepts (like a person) or grammatical constructs
    /// (like a noun phrase or verb).
    /// </para>
    /// <para>
    /// Generally, to indicate a value is intended to be rendered via Spiel in some way, its type
    /// should be <see cref="SpielArgument"/>. For example:
    /// <code>
    /// public interface IRecord
    /// {
    ///     SpielArgument Description { get; }
    /// }
    /// </code>
    /// </para>
    /// <para>
    /// Types which intended for use by the Spiel API should generally define an implicit
    /// conversion as follows:
    /// <code>
    /// public static implicit operator SpielArgument(SpielArgumentValue spielArgumentValue)
    /// {
    ///     return new SpielArgument(spielArgumentValue);
    /// }
    /// </code>
    /// This allows arguments to be easily passed to the Spiel API like so:
    /// <code>
    /// Render("string", spielArgumentValue, spielArgumentValue, "another string");
    /// </code>
    /// </para>
    /// <para>
    /// The Spiel rendering system handles a wide range of data using the same mechanisms.
    /// Abstract concepts, events, sentences, objects, text, intermediate data (e.g. a verb
    /// whose case has not yet been defined), and others all share the same basic rendering
    /// pipeline. As a result, it is impractical to represent these with different interfaces.
    /// And so, they are all wrapped using the <see cref="SpielArgument"/> type.
    /// </para>
    /// </summary>
    [Serializable]
    [DebuggerDisplay("{Value}")]
    public struct SpielArgument
    {
        [DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
        public object Value { get; }

        public SpielArgument(string value)
        {
            Value = value ?? throw new ArgumentNullException(nameof(value));
        }

        public SpielArgument(ISpielArgumentValue value)
        {
            Value = value ?? throw new ArgumentNullException(nameof(value));
        }

        public static implicit operator SpielArgument(string value)
        {
            return new SpielArgument(value ?? throw new ArgumentNullException(nameof(value)));
        }

        public override string ToString()
        {
            return Value.ToString();
        }

        public string ToRenderedString()
        {
            return Value is ISpielArgumentValue value ? value.ToRenderString() : Value.ToString();
        }
    }
}
