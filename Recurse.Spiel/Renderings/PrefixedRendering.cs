﻿using Recurse.Spiel.English;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Recurse.Spiel.Renderings
{
    [Serializable]
    public class PrefixedRendering : IRendering, ISpielArgumentValue, INumbered
    {
        private readonly IEnumerable<SpielArgument> _prefixes;

        private readonly IEnumerable<SpielArgument> _arguments;

        public bool IsPlural => Grammar.GetPlurality(_arguments);

        public PrefixedRendering(SpielArgument prefix, IEnumerable<SpielArgument> arguments)
        {
            _prefixes = new[] { prefix };
            _arguments = arguments;
        }

        public PrefixedRendering(IEnumerable<SpielArgument> prefixes, IEnumerable<SpielArgument> arguments)
        {
            _prefixes = prefixes;
            _arguments = arguments;
        }

        public override string ToString()
        {
            return ToRenderString();
        }

        public string ToRenderString()
        {
            return string.Concat(_prefixes.Concat(_arguments).Select(e => e.ToRenderedString()));
        }

        public IEnumerable<SpielArgument> GetRenderedElements()
        {
            foreach (var prefix in _prefixes)
            {
                yield return prefix;
            }

            foreach (var argument in _arguments)
            {
                yield return argument;
            }
        }

        public static implicit operator SpielArgument(PrefixedRendering prefixedRendering)
        {
            return new SpielArgument(prefixedRendering);
        }
    }
}
