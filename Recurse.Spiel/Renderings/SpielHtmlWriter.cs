﻿using Recurse.Spiel.Aliases;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Recurse.Spiel.Renderings
{
    /// <summary>
    /// A <see cref="SpielWriter"/> which renders aliases as HTML links.
    /// </summary>
    [Serializable]
    public abstract class SpielHtmlWriter : SpielWriter
    {
        private readonly Dictionary<object, string> _urls = new Dictionary<object, string>();

        private bool _isWritingLinkBody;

        public override void Write(SpielArgument argument)
        {
            if (argument.Value is IAliasRendering alias && alias.Referents.Count == 1)
            {
                Write(alias.Referents.Single(), argument);
            }
            else
            {
                Write(argument, argument);
            }
        }

        protected abstract string GetUrl(SpielArgument referent);

        private void Write(SpielArgument referent, SpielArgument argument)
        {
            string linkUrl;
            if (!_isWritingLinkBody)
            {
                if (!_urls.TryGetValue(referent, out linkUrl))
                {
                    linkUrl = GetUrl(referent);
                    if (linkUrl != null)
                    {
                        _urls[referent] = linkUrl;
                    }
                }
            }
            else
            {
                linkUrl = null;
            }

            if (linkUrl != null)
            {
                try
                {
                    _isWritingLinkBody = true;

                    WriteMarkup($"<a href='{linkUrl}'>");
                    base.Write(argument);
                    WriteMarkup("</a>");
                }
                finally
                {
                    _isWritingLinkBody = false;
                }
            }
            else
            {
                base.Write(argument);
            }
        }
    }
}
