﻿using System.Collections.Generic;

namespace Recurse.Spiel.Renderings
{
    /// <summary>
    /// Interface for a rendered <see cref="SpielArgument"/> which contains
    /// other rendered <see cref="SpielArgument"/>s.
    /// </summary>
    public interface IRendering : ISpielArgumentValue
    {
        IEnumerable<SpielArgument> GetRenderedElements();
    }
}
