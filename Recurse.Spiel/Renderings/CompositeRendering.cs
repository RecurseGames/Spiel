﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Recurse.Spiel.Renderings
{
    [Serializable]
    public class CompositeRendering : IRendering, ISpielArgumentValue
    {
        [DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
        public IEnumerable<SpielArgument> Elements { get; }

        public CompositeRendering(IEnumerable<SpielArgument> elements)
        {
            Elements = elements;
        }

        public string ToRenderString()
        {
            return string.Concat(Elements.Select(e => e.ToRenderedString()));
        }

        public IEnumerable<SpielArgument> GetRenderedElements()
        {
            return Elements;
        }

        public static implicit operator SpielArgument(CompositeRendering rendering)
        {
            return new SpielArgument(rendering);
        }
    }
}
