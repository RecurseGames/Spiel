﻿using Recurse.Spiel.Renderings;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Recurse.Spiel.Delimeters
{
    [Serializable]
    public class SentenceDelimiter : IDelimiter
    {
        public static SentenceDelimiter Default { get; } = new SentenceDelimiter(". ", ".");

        public static SentenceDelimiter Newline { get; } = new SentenceDelimiter("\n", null);

        private string Delimiter { get; }

        private string Suffix { get; }

        private SentenceDelimiter(string delimiter, string suffix)
        {
            Delimiter = delimiter;
            Suffix = suffix;
        }

        public SpielArgument Delimit(IEnumerable<SpielArgument> elements)
        {
            return new SpielArgument(new List(this, elements.ToArray()));
        }

        public SpielArgument Delimit(IEnumerable<string> elements)
        {
            return new SpielArgument(new List(this, elements.Select(e => new SpielArgument(e)).ToArray()));
        }

        [Serializable]
        private class List : IRendering, ISpielArgumentValue
        {
            private readonly SentenceDelimiter _delimiter;

            [DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
            private readonly SpielArgument[] _sentences;

            public List(SentenceDelimiter delimiter, params SpielArgument[] sentences)
            {
                _delimiter = delimiter;
                _sentences = sentences;
            }

            public string ToRenderString()
            {
                return string.Concat(GetRenderedElements().Select(e => e.ToRenderedString()));
            }

            public override string ToString()
            {
                return string.Concat(GetRenderedElements().Select(e => e.ToRenderedString()));
            }

            public IEnumerable<SpielArgument> GetRenderedElements()
            {
                for (var i = 0; i < _sentences.Length; i++)
                {
                    if (i != 0)
                    {
                        yield return _delimiter.Delimiter;
                    }

                    yield return new SentenceRendering(_sentences[i]);
                }

                if (_delimiter.Suffix != null)
                {
                    yield return _delimiter.Suffix;
                }
            }
        }
    }
}
