﻿using System;
using System.Collections.Generic;

namespace Recurse.Spiel.Renderings
{
    [Serializable]
    public class SentenceRendering : IRendering, ISpielArgumentValue
    {
        private readonly SpielArgument _body;

        public SentenceRendering(SpielArgument body)
        {
            _body = body;
        }

        public IEnumerable<SpielArgument> GetRenderedElements()
        {
            yield return _body;
        }

        public string ToRenderString()
        {
            return _body.ToRenderedString();
        }

        public override string ToString()
        {
            return $"{GetType().Name}{_body.ToRenderedString()}";
        }

        public static implicit operator SpielArgument(SentenceRendering rendering)
        {
            return new SpielArgument(rendering);
        }
    }
}
