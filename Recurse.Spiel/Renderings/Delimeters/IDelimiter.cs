﻿using System.Collections.Generic;

namespace Recurse.Spiel.Delimeters
{
    public interface IDelimiter
    {
        SpielArgument Delimit(IEnumerable<SpielArgument> elements);

        SpielArgument Delimit(IEnumerable<string> elements);
    }
}
