﻿using Recurse.Spiel.English;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Recurse.Spiel.Renderings
{
    [Serializable]
    public class ListRendering : IRendering, ISpielArgumentValue, INumbered
    {
        private readonly ListDelimiter _type;

        [DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
        public IEnumerable<SpielArgument> Elements { get; }

        public bool IsPlural => Grammar.GetPlurality(Elements);

        public ListRendering(ListDelimiter type, IEnumerable<SpielArgument> elements)
        {
            _type = type;

            Elements = elements;
        }

        public override string ToString()
        {
            return ToRenderString();
        }

        public string ToRenderString()
        {
            return string.Concat(GetRenderedElements().Select(e => e.ToRenderedString()));
        }

        public IEnumerable<SpielArgument> GetRenderedElements()
        {
            int elementCount = 0;
            var lastElement = default(SpielArgument);
            var secondLastElement = default(SpielArgument);
            foreach (var element in Elements)
            {
                elementCount++;
                if (elementCount == 3)
                {
                    yield return secondLastElement;
                }
                else if (elementCount > 3)
                {
                    yield return _type.Delimiter;
                    yield return secondLastElement;
                }

                secondLastElement = lastElement;
                lastElement = element;
            }

            if (elementCount == 1)
            {
                yield return lastElement;
            }
            else if (elementCount == 2)
            {
                yield return secondLastElement;
                yield return _type.PairDelimiter;
                yield return lastElement;
            }
            else if (elementCount >= 3)
            {
                yield return _type.Delimiter;
                yield return secondLastElement;
                yield return _type.FinalDelimiter;
                yield return lastElement;
            }
        }

        public static implicit operator SpielArgument(ListRendering rendering)
        {
            return new SpielArgument(rendering);
        }
    }
}
