﻿using Recurse.Spiel.Delimeters;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Recurse.Spiel.Renderings
{
    [Serializable]
    public class ListDelimiter : IDelimiter
    {
        public static ListDelimiter OxfordComma => new ListDelimiter(", ", ", and ", " and ");

        public static ListDelimiter Comma => new ListDelimiter(", ");

        public SpielArgument Delimiter { get; }

        public SpielArgument FinalDelimiter { get; }

        public SpielArgument PairDelimiter { get; }

        public ListDelimiter(SpielArgument delimiter)
        {
            Delimiter = delimiter;
            FinalDelimiter = delimiter;
            PairDelimiter = delimiter;
        }

        public ListDelimiter(SpielArgument delimiter, SpielArgument finalDelimiter, SpielArgument pairDelimiter)
        {
            Delimiter = delimiter;
            FinalDelimiter = finalDelimiter;
            PairDelimiter = pairDelimiter;
        }

        public SpielArgument Delimit(IEnumerable<ISpielArgumentValue> elements)
        {
            return new ListRendering(this, elements.Select(element => new SpielArgument(element)));
        }

        public SpielArgument Delimit(IEnumerable<string> elements)
        {
            return new ListRendering(this, elements.Select(e => new SpielArgument(e)));
        }

        public SpielArgument Delimit(IEnumerable<SpielArgument> elements)
        {
            return new ListRendering(this, elements);
        }

        public override string ToString()
        {
            return $"1{Delimiter}1{PairDelimiter}2{FinalDelimiter}3";
        }
    }
}
