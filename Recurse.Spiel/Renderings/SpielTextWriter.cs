﻿using System;

namespace Recurse.Spiel.Renderings
{
    [Serializable]
    public class SpielTextWriter : SpielWriter
    {
        public string Text { get; private set; }

        public override void Clear()
        {
            base.Clear();

            Text = string.Empty;
        }

        protected override void OnWrite(string text)
        {
            Text += text;
        }
    }
}
