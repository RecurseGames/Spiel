﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Recurse.Spiel.Renderings
{
    /// <summary>
    /// Given a list of rendered <see cref="SpielArgument"/>s, outputs
    /// them as text. This class can be used to convert Spiel output to
    /// strings, HTML, UI elements, and other formats.
    /// </summary>
    [Serializable]
    public abstract class SpielWriter
    {
        private bool _capitaliseNextString;

        public virtual void Clear()
        {
            _capitaliseNextString = false;
        }

        public void Write(IEnumerable<SpielArgument> elements)
        {
            if (elements == null)
            {
                throw new ArgumentNullException(nameof(elements));
            }

            foreach (var element in elements)
            {
                Write(element);
            }
        }

        public virtual void Write(SpielArgument argument)
        {
            if (argument.Value is SentenceRendering sentenceRendering)
            {
                _capitaliseNextString = true;

                foreach (var child in sentenceRendering.GetRenderedElements())
                {
                    Write(child);
                }
            }
            else if (argument.Value is IRendering rendering)
            {
                foreach (var child in rendering.GetRenderedElements())
                {
                    Write(child);
                }
            }
            else
            {
                var text = argument.ToRenderedString();

                Write(text);
            }
        }

        public void WriteMarkup(string markup)
        {
            OnWrite(markup);
        }

        protected abstract void OnWrite(string text);

        private void Write(string text)
        {
            if (_capitaliseNextString && text.Length != 0)
            {
                var capitalisedText = text.First().ToString().ToUpper() + text.Substring(1);

                _capitaliseNextString = false;

                OnWrite(capitalisedText);
            }
            else
            {
                OnWrite(text);
            }
        }
    }
}
