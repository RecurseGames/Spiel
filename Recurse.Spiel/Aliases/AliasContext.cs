﻿using Recurse.Core.Collections;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Recurse.Spiel.Aliases
{
    /// <summary>
    /// Represents a context in which aliases can be used.
    /// </summary>
    [Serializable]
    public class AliasContext
    {
        private readonly IDictionary<IAlias, IAliasRendering> _usableAliases = new Dictionary<IAlias, IAliasRendering>();

        /// <summary>
        /// Gets all aliases which can be used in this context (i.e., they exist and are unambiguous)
        /// </summary>
        public IEnumerable<IAliasRendering> UsableAliases => _usableAliases.Values.Where(alias => alias != null);

        /// <summary>
        /// Gets the usable (i.e., it exists and is unambiguous) alias of the given type
        /// </summary>
        public IAliasRendering GetUsableAlias(IAlias type)
        {
            if (type == null)
            {
                throw new ArgumentNullException(nameof(type));
            }

            _usableAliases.TryGetValue(type, out IAliasRendering alias);
            return alias;
        }

        /// <summary>
        /// Indicates the given alias exists in this context. If no other
        /// aliases exist with the same type, it can be used without being
        /// ambiguous.
        /// </summary>
        public void Add(IAliasRendering alias)
        {
            if (alias == null)
            {
                throw new ArgumentNullException(nameof(alias));
            }

            if (_usableAliases.TryGetValue(alias.Alias, out IAliasRendering existingAlias))
            {
                if (existingAlias != null)
                {
                    _usableAliases[alias.Alias] = MultisetComparer.Equals(alias.Referents, existingAlias.Referents)
                        ? alias
                        : null;
                }
            }
            else
            {
                _usableAliases[alias.Alias] = alias;
            }
        }

        public void Clear()
        {
            _usableAliases.Clear();
        }
    }
}
