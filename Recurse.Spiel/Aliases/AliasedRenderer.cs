﻿using System;

namespace Recurse.Spiel.Aliases
{
#warning Can improve naming. This renders the most-specified alias, and adds aliases to the context
    /// <summary>
    /// Converts arguments into aliases, when supported, and adds
    /// those aliases to the current context so they can be re-used.
    /// </summary>
    [Serializable]
    public class AliasedRenderer : IArgumentTransform
    {
        private readonly AliasContext _context;

        public AliasedRenderer(AliasContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public SpielArgument TransformArgument(SpielArgument argument)
        {
            if (argument.Value is IAliased aliased)
            {
                return aliased.GetAlias(_context);
            }
            else
            {
                return argument;
            }
        }
    }
}
