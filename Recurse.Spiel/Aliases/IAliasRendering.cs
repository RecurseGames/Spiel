﻿using System.Collections.Generic;

namespace Recurse.Spiel.Aliases
{
    /// <summary>
    /// A reference to one or more referents by a given alias. For example, "he/him"
    /// referring to a person called John.
    /// </summary>
    public interface IAliasRendering : ISpielArgumentValue
    {
        IAlias Alias { get; }

        IReadOnlyCollection<SpielArgument> Referents { get; }
    }
}
