﻿using Recurse.Spiel.Aliases;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Recurse.Spiel.References
{
    /// <summary>
    /// Wrapper around another argument which allows it to
    /// be represented using aliases.
    /// </summary>
    [Serializable]
    public class AliasIdentity : IAliased, ISpielArgumentValue
    {
        private SpielArgument Referent { get; }

        /// <summary>
        /// Factories which create aliases for the given argument, ordered
        /// from most to least specific (e.g., john smith, john, he/him).
        /// </summary>
        public IList<IAlias> AliasFactories { get; } = new List<IAlias>();

        public AliasIdentity(SpielArgument referent)
        {
            Referent = referent;
        }

        public SpielArgument GetAlias(AliasContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            foreach (var aliasType in AliasFactories.Reverse())
            {
                var alias = aliasType.CreateAlias(Referent);
                context.Add(alias);
            }

            return AliasFactories
                .Select(e => new SpielArgument(e.CreateAlias(Referent)))
                .DefaultIfEmpty(Referent)
                .First();
        }

        public override string ToString()
        {
            var renderedAliases = string.Join(", ", AliasFactories.Select(e => e.CreateAlias(Referent).ToRenderString()));

            return $"Reference({renderedAliases})";
        }

        public string ToRenderString()
        {
            return AliasFactories.First().CreateAlias(Referent).ToRenderString();
        }

        public static implicit operator SpielArgument(AliasIdentity identity)
        {
            return new SpielArgument(identity);
        }
    }
}
