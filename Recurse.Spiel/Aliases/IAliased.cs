﻿namespace Recurse.Spiel.Aliases
{
    /// <summary>
    /// An argument which can be referred to by aliases.
    /// </summary>
    public interface IAliased
    {
        /// <summary>
        /// Gets an alias referring to the argument, assuming
        /// it hasn't been referred to already, and adds aliases
        /// which can be used from that point onwards to the
        /// given context.
        /// </summary>
        SpielArgument GetAlias(AliasContext context);
    }
}
