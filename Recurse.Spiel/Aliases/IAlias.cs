﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;

namespace Recurse.Spiel.Aliases
{
    /// <summary>
    /// An alias which can refer to one or more referents. This could be
    /// a name or set of pronouns. In a given context, the same alias can
    /// only refer to one set of referents without creating ambiguity.
    /// </summary>
    public interface IAlias : IEquatable<IAlias>
    {
        /// <summary>
        /// Creates an alias which refers to the given arguments.
        /// </summary>
        IAliasRendering CreateAlias(params SpielArgument[] referents);
    }
}
