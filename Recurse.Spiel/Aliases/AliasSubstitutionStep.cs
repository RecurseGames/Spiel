﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Recurse.Spiel.Aliases
{
    /// <summary>
    /// Replaces arguments with their aliases.
    /// </summary>
    [Serializable]
    public class AliasSubstitutionStep : IArgumentsTransform
    {
        private readonly AliasContext _aliasContext;

        public AliasSubstitutionStep(AliasContext aliasContext)
        {
            _aliasContext = aliasContext ?? throw new ArgumentNullException(nameof(aliasContext));
        }

        public IEnumerable<SpielArgument> TransformArguments(IReadOnlyCollection<SpielArgument> arguments)
        {
            if (arguments == null)
            {
                throw new ArgumentNullException(nameof(arguments));
            }

            var aliases = _aliasContext.UsableAliases.OrderByDescending(alias => alias.Referents.Count);
            var remainingArguments = arguments.ToList();
            var result = new List<SpielArgument>();
            foreach (var alias in aliases)
            {
                if (alias.Referents.All(remainingArguments.Contains))
                {
                    result.Add(new SpielArgument(alias));
                    foreach (var aliasReferent in alias.Referents)
                    {
                        remainingArguments.Remove(aliasReferent);
                    }
                }
            }

            foreach (var unaliasedArgument in remainingArguments.ToArray())
            {
                result.Add(unaliasedArgument);
            }

            return result;
        }
    }
}
