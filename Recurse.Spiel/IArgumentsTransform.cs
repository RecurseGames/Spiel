﻿using System.Collections.Generic;

namespace Recurse.Spiel
{
    public interface IArgumentsTransform
    {
        IEnumerable<SpielArgument> TransformArguments(IReadOnlyCollection<SpielArgument> arguments);
    }
}
