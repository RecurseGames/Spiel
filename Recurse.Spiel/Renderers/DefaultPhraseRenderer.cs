﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using Recurse.Spiel.Aliases;
using Recurse.Spiel.English;
using Recurse.Spiel.English.Phrases;
using Recurse.Spiel.Groupings;

namespace Recurse.Spiel.Renderers
{
    /// <summary>
    /// Default phrase renderer which supports all built-in Spiel types.
    /// </summary>
    [Serializable]
    public class DefaultPhraseRenderer : IPhraseRenderer
    {
        private readonly ISentenceRenderer _clauseRenderer;

        private readonly AliasContext _fixedAliases;

        private readonly AliasContext _transientPronouns;

        private readonly IPhraseElementContext _elementContext;

        public DefaultPhraseRenderer(
            ISentenceRenderer clauseRenderer,
            AliasContext fixedAliases,
            AliasContext transientPronouns)
        {
            _clauseRenderer = clauseRenderer;
            _fixedAliases = fixedAliases;
            _transientPronouns = transientPronouns;
            _elementContext = ObjectContextRoot.Instance;
        }

        private DefaultPhraseRenderer(
            ISentenceRenderer clauseRenderer, 
            AliasContext fixedAliases,
            AliasContext transientPronouns, 
            IPhraseElementContext objectContext)
        {
            _clauseRenderer = clauseRenderer;
            _fixedAliases = fixedAliases;
            _transientPronouns = transientPronouns;
            _elementContext = new ObjectContextChild(objectContext);
        }

        public IEnumerable<SpielArgument> RenderPhrase(SpielArgumentList elements)
        {
            var renderer = CreateClauseRenderer();
            return renderer.GetTransformedArguments(elements);
        }

        public IEnumerable<SpielArgument> RenderObjects(ObjectDeclension declension, IEnumerable<SpielArgument> objects)
        {
            var renderer = CreateObjectRenderer(declension, objects);
            var objectList = SpielArgumentList.Create(SpielArgumentListType.Collection, objects);
            return renderer.GetTransformedArguments(objectList);
        }

        public CompositeTransform CreateClauseRenderer()
        {
            var renderer = new CompositeTransform();
            var renderSubjectStep = new RenderSubjectStep(this, _elementContext);
            var subordinateRenderer = new DefaultPhraseRenderer(_clauseRenderer, _fixedAliases, _transientPronouns, renderSubjectStep.ObjectContext);

            // Alias substitutions at this level support "I know he danced" -> "I know that"
            renderer.InitialSteps.Add(new AliasSubstitutionStep(_transientPronouns));
            renderer.InitialSteps.Add(new AliasSubstitutionStep(_fixedAliases));

            renderer.InitialSteps.Add(new OrderedMergeRenderStep());

            renderer.InitialSteps.Add(renderSubjectStep);

            renderer.ElementRenderers.Add(new ClauseContainerTransform(_clauseRenderer));
            renderer.ElementRenderers.Add(new SpielPhraseTransform(subordinateRenderer));
            renderer.ElementRenderers.Add(new PhraseElementCompositeTransform(subordinateRenderer));
            renderer.ElementRenderers.Add(new PhraseElementTransform(renderSubjectStep.ObjectContext, ObjectDeclension.Object));

            return renderer;
        }

        public CompositeTransform CreateObjectRenderer(ObjectDeclension declension, IEnumerable<SpielArgument> objects)
        {
            var renderer = new CompositeTransform();

            var initialObjects = objects.ToList();

            renderer.InitialSteps.Add(new AliasSubstitutionStep(_transientPronouns));
            renderer.InitialSteps.Add(new AliasSubstitutionStep(_fixedAliases));
            renderer.InitialSteps.Add(new UnorderedMergeRenderStep());

            renderer.ElementRenderers.Add(new IdentifiedRenderer());
            renderer.ElementRenderers.Add(new ClauseContainerTransform(_clauseRenderer));
            renderer.ElementRenderers.Add(new PhraseElementCompositeTransform(this));
            renderer.ElementRenderers.Add(new SpielPhraseTransform(this));
            renderer.ElementRenderers.Add(new RegisterObjectPronounsStep(_transientPronouns));
            renderer.ElementRenderers.Add(new AliasedRenderer(_transientPronouns));
            renderer.ElementRenderers.Add(new PhraseElementTransform(_elementContext, declension));
            renderer.ElementRenderers.Add(new A.Renderer(this));

#warning Ensure required
            renderer.FinalSteps.Add(new UnorderedMergeRenderStep());

            renderer.FinalSteps.Add(new OrderingRenderStep());
            renderer.FinalSteps.Add(new CollectiveAliasDeclarationStep(initialObjects, _transientPronouns, _fixedAliases));

            return renderer;
        }
    }
}
