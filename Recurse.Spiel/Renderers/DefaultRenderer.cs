﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;
using System.Collections.Generic;
using Recurse.Spiel.Aliases;
using Recurse.Spiel.Delimeters;
using Recurse.Spiel.English;
using Recurse.Spiel.English.Phrases;
using Recurse.Spiel.Groupings;
using Recurse.Spiel.Renderings;

namespace Recurse.Spiel.Renderers
{
    /// <summary>
    /// Default sentence renderer which supports all built-in Spiel types.
    /// </summary>
    [Serializable]
    public class DefaultRenderer : ISentenceRenderer
    {
        private AliasContext _fixedAliases;

        private AliasContext _transientPronouns;

        public DefaultRenderer()
        {
            _fixedAliases = new AliasContext();
            _transientPronouns = new AliasContext();
        }

        public void AddAliasRendering(IAliasRendering aliasRendering)
        {
            _fixedAliases.Add(aliasRendering);
        }

        /// <summary>
        /// Resets short-lived pronouns, allowing them to be used to refer
        /// to new referents without ambiguity. Generally, pronouns like
        /// he/him are short-lived, whereas you/you is fixed for a given context.
        /// </summary>
        public void ClearTransientPronouns()
        {
            _transientPronouns.Clear();
        }

        public IEnumerable<SpielArgument> RenderSentences(IEnumerable<SpielArgument> clauses)
        {
            var renderer = CreateClausesRenderer();
            var clauseList = SpielArgumentList.Create(SpielArgumentListType.Collection, clauses);
            return renderer.GetTransformedArguments(clauseList);
        }

        public IEnumerable<SpielArgument> RenderClauses(params SpielArgument[] clauses)
        {
            var renderer = CreateClausesRenderer();
            var clauseList = SpielArgumentList.Create(SpielArgumentListType.Collection, clauses);
            return renderer.GetTransformedArguments(clauseList);
        }

        public CompositeTransform CreateObjectRenderer(params SpielArgument[] clauses)
        {
            var elementRenderer = new DefaultPhraseRenderer(this, _fixedAliases, _transientPronouns);
            return elementRenderer.CreateObjectRenderer(ObjectDeclension.Subject, clauses);
        }

        public SpielArgument RenderParagraph(params SpielArgument[] sentences)
        {
            var renderedSentences = RenderClauses(sentences);
            return SentenceDelimiter.Default.Delimit(renderedSentences);
        }

        public string RenderHeading(SpielArgument heading)
        {
            var renderer = CreateClausesRenderer();
            var elements = SpielArgumentList.Create(SpielArgumentListType.Collection, heading);
            var renderedElements = renderer.GetTransformedArguments(elements);
            return new CompositeRendering(renderedElements).ToRenderString();
        }

        private CompositeTransform CreateClausesRenderer()
        {
            var renderer = new CompositeTransform();
            var elementRenderer = new DefaultPhraseRenderer(this, _fixedAliases, _transientPronouns);

            renderer.InitialSteps.Add(new OrderedMergeRenderStep());

#warning Rename SpielString to SpielClause, and related changes
#warning Decide whether suffix is Step, Transformation, Rule, or other
            renderer.ElementRenderers.Add(new IdentifiedRenderer());
            renderer.ElementRenderers.Add(new ClauseContainerTransform(this));
            renderer.ElementRenderers.Add(new SpielPhraseTransform(elementRenderer)); // renders clauses
            renderer.ElementRenderers.Add(new AliasedRenderer(_transientPronouns));
            renderer.ElementRenderers.Add(new PhraseElementCompositeTransform(elementRenderer)); // renders weird They/Them-as-sentence
            renderer.ElementRenderers.Add(new A.Renderer(elementRenderer));

            return renderer;
        }
    }
}
