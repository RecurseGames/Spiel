﻿namespace Recurse.Spiel.Renderers
{
    /// <summary>
    /// Interface for arguments identified by another argument.
    /// </summary>
    public interface IIdentified : ISpielArgumentValue
    {
        SpielArgument Identity { get; }
    }
}
