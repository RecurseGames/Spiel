﻿using Recurse.Spiel.Renderers;

namespace Recurse.Spiel.Groupings
{
    /// <summary>
    /// Interface for arguments which render themselves given a renderer.
    /// </summary>
    public interface IClauseContainer : ISpielArgumentValue
    {
        SpielArgument Render(ISentenceRenderer renderer);
    }
}
