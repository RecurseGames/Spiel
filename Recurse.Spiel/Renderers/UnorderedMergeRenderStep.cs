﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;
using System.Collections.Generic;
using System.Linq;

namespace Recurse.Spiel
{
    [Serializable]
    public class UnorderedMergeRenderStep : IArgumentsTransform
    {
        public IEnumerable<SpielArgument> TransformArguments(IReadOnlyCollection<SpielArgument> arguments)
        {
            if (arguments == null)
            {
                throw new ArgumentNullException(nameof(arguments));
            }

            var argumentList = arguments.ToList();
            var i = 0;
            var j = 1;
            while (i < argumentList.Count)
            {
                if (j < argumentList.Count)
                {
                    var current = argumentList[i].Value as ISequenceMergable;
                    var other = argumentList[j].Value as ISequenceMergable;
                    var merged = current?.GetMerged(other);
                    if (merged != null)
                    {
                        argumentList[i] = new SpielArgument(merged);
                        argumentList.RemoveAt(j);
                        i = 0;
                        j = 1;
                    }
                    else
                    {
                        j++;
                    }
                }
                else
                {
                    i++;
                    j = i + 1;
                }
            }
            return argumentList;
        }
    }
}
