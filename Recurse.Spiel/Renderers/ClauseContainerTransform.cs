﻿using Recurse.Spiel.Groupings;
using System;

namespace Recurse.Spiel.Renderers
{
    [Serializable]
    public class ClauseContainerTransform : IArgumentTransform
    {
        private readonly ISentenceRenderer _renderer;

        public ClauseContainerTransform(ISentenceRenderer renderer)
        {
            _renderer = renderer ?? throw new ArgumentNullException(nameof(renderer));
        }

        public SpielArgument TransformArgument(SpielArgument argument)
        {
            if (argument.Value is IClauseContainer renderable)
            {
                return renderable.Render(_renderer);
            }
            else
            {
                return argument;
            }
        }
    }
}
