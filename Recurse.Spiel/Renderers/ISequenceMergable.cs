﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace Recurse.Spiel
{
    /// <summary>
    /// Interface for arguments which can be merged with other arguments in a sequence.
    /// </summary>
    public interface ISequenceMergable : ISpielArgumentValue
    {
        ISequenceMergable GetMerged(ISequenceMergable next);
    }
}
