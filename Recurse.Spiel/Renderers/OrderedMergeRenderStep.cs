﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System.Collections.Generic;
using System;
using System.Linq;

namespace Recurse.Spiel
{
    [Serializable]
    public class OrderedMergeRenderStep : IArgumentsTransform
    {
        public IEnumerable<SpielArgument> TransformArguments(IReadOnlyCollection<SpielArgument> arguments)
        {
            if (arguments == null)
            {
                throw new ArgumentNullException(nameof(arguments));
            }

            var argumentList = arguments.ToList();
            var i = 0;
            while (i < argumentList.Count - 1)
            {
                var current = argumentList[i].Value as ISequenceMergable;
                var next = argumentList[i + 1].Value as ISequenceMergable;
                var merged = current?.GetMerged(next);
                if (merged != null)
                {
                    argumentList[i] = new SpielArgument(merged);
                    argumentList.RemoveAt(i + 1);
                    i = Math.Max(0, i - 1);
                }
                else
                {
                    i++;
                }
            }
            return argumentList;
        }
    }
}
