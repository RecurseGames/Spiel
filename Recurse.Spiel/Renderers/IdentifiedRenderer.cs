﻿using Recurse.Spiel.Renderers;

namespace Recurse.Spiel.Groupings
{
    public class IdentifiedRenderer : IArgumentTransform
    {
        public SpielArgument TransformArgument(SpielArgument argument)
        {
            var identified = argument.Value as IIdentified;
            if (identified != null)
            {
                return identified.Identity;
            }
            else
            {
                return argument;
            }
        }
    }
}
