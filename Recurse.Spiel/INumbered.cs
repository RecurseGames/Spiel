﻿namespace Recurse.Spiel
{
    /// <summary>
    /// Interface for arguments which may have grammatical number (e.g., are
    /// singular or plural). By default, elements are assumed to be singular.
    /// </summary>
    public interface INumbered
    {
        bool IsPlural { get; }
    }
}
