﻿using Recurse.Core.Exceptions;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Recurse.Spiel
{
    /// <summary>
    /// A collection of <see cref="SpielArgument"/> instances. Unlike classes
    /// like <see cref="List{SpielArgument}"/>, the string representation
    /// of this class consists of its elements, which is useful
    /// for debugging. This class also ensures any <see cref="IEnumerable{SpielArgument}"/>
    /// instances are fully enumerated (as nested enumeration is difficult to debug).
    /// </summary>
    public abstract class SpielArgumentList : IReadOnlyList<SpielArgument>, IList<SpielArgument>
    {
        [DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
        private IReadOnlyList<SpielArgument> _readOnlyElements;

        private IList<SpielArgument> _elements;

        public abstract SpielArgumentListType Type { get; }

        public SpielArgument this[int index] 
        {
            get
            { 
                return _readOnlyElements[index];
            }
            set
            {
                if (Equals(value, default(SpielArgument)))
                {
                    throw new ArgumentException("Default argument is not valid.", nameof(value));
                }
                else if (IsReadOnly)
                {
                    throw CreateReadOnlyException();
                }

                _elements[index] = value;
            }
        }

        public int Count => _readOnlyElements.Count;

        public bool IsReadOnly => _elements == null || _elements.IsReadOnly;

        protected SpielArgumentList(IReadOnlyList<SpielArgument> elements)
        {
            if (elements.Contains(default(SpielArgument)))
            {
                throw new ArgumentException("Default arguments are not valid.", nameof(elements));
            }

            _readOnlyElements = elements;
            _elements = elements as IList<SpielArgument>;
        }

        public static SpielArgumentList Create(SpielArgumentListType type)
        {
            switch (type)
            {
                case SpielArgumentListType.Collection:
                    return new Objects(new List<SpielArgument>());
                case SpielArgumentListType.Sequence:
                    return new Sequence(new List<SpielArgument>());
                default:
                    throw new ArgumentException("Type not recognised.", nameof(type));
            }
        }

        public static SpielArgumentList Create(SpielArgumentListType type, params SpielArgument[] elements)
        {
            return CreateNew(type, elements ?? throw new ArgumentNullException(nameof(elements)));
        }

        public static SpielArgumentList Create(SpielArgumentListType type, IEnumerable<SpielArgument> backing)
        {
            if (backing is SpielArgumentList list && Equals(list.Type, type))
            {
                return list;
            }
            else if (backing is IReadOnlyList<SpielArgument> collection)
            {
                return CreateNew(type, collection);
            }
            else
            {
                return CreateNew(type, backing.ToArray());
            }
        }

        public static SpielArgumentList CreateNonEmpty(SpielArgumentListType type, IEnumerable<SpielArgument> backing)
        {
            if (backing == null)
            {
                return null;
            }
            else if (backing is SpielArgumentList list && Equals(list.Type, type))
            {
                return list.Count != 0 ? list : null;
            }
            else
            {
                var backingList = backing as IReadOnlyList<SpielArgument> ?? backing.ToArray();

                return backingList.Count != 0 ? CreateNew(type, backingList) : null;
            }
        }

        public static SpielArgumentList CreateNonEmpty(SpielArgumentListType type, params SpielArgument[] backing)
        {
            if (backing == null || backing.Length == 0)
            {
                return null;
            }
            else
            {
                return CreateNew(type, backing);
            }
        }

        private static SpielArgumentList CreateNew(SpielArgumentListType type, IReadOnlyList<SpielArgument> elements)
        {
            switch (type)
            {
                case SpielArgumentListType.Collection:
                    return new Objects(elements);
                case SpielArgumentListType.Sequence:
                    return new Sequence(elements);
                default:
                    throw new ArgumentException("Type not recognised.", nameof(type));
            }
        }

        private static NotSupportedException CreateReadOnlyException()
        {
            return new NotSupportedException("Collection is read-only.");
        }

        public override string ToString()
        {
            switch (Type)
            {
                case SpielArgumentListType.Collection:
                    return string.Join(", ", _readOnlyElements);
                case SpielArgumentListType.Sequence:
                    return string.Concat(_readOnlyElements);
                default:
                    throw new UnexpectedEnumException(Type);
            }
        }

        public IEnumerator<SpielArgument> GetEnumerator()
        {
            return _readOnlyElements.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _readOnlyElements.GetEnumerator();
        }

        public int IndexOf(SpielArgument item)
        {
            EnsureElements();
            return _elements.IndexOf(item);
        }

        public void Insert(int index, SpielArgument item)
        {
            if (Equals(item, default(SpielArgument)))
            {
                throw new ArgumentException("Default argument is not valid.", nameof(item));
            }
            else if (IsReadOnly)
            {
                throw CreateReadOnlyException();
            }

            _elements.Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            if (IsReadOnly)
            {
                throw CreateReadOnlyException();
            }

            _elements.RemoveAt(index);
        }

        public void Add(SpielArgument item)
        {
            if (Equals(item, default(SpielArgument)))
            {
                throw new ArgumentException("Default argument is not valid.", nameof(item));
            }
            else if (IsReadOnly)
            {
                throw CreateReadOnlyException();
            }

            _elements.Add(item);
        }

        public void Clear()
        {
            if (IsReadOnly)
            {
                throw CreateReadOnlyException();
            }

            _elements.Clear();
        }

        public bool Contains(SpielArgument item)
        {
            return _elements != null ? _elements.Contains(item) : _readOnlyElements.Contains(item);
        }

        public void CopyTo(SpielArgument[] array, int arrayIndex)
        {
            EnsureElements();
            _elements.CopyTo(array, arrayIndex);
        }

        public bool Remove(SpielArgument item)
        {
            if (IsReadOnly)
            {
                throw CreateReadOnlyException();
            }

            return _elements.Remove(item);
        }

        private void EnsureElements()
        {
            if (_elements == null)
            {
                var readOnlyElements = _readOnlyElements.ToList();

                _readOnlyElements = readOnlyElements;
                _elements = readOnlyElements.AsReadOnly();
            }
        }

        private class Objects : SpielArgumentList
        {
            public override SpielArgumentListType Type => SpielArgumentListType.Collection;

            public Objects(IReadOnlyList<SpielArgument> elements) : base(elements)
            {
            }
        }

        private class Sequence : SpielArgumentList
        {
            public override SpielArgumentListType Type => SpielArgumentListType.Sequence;

            public Sequence(IReadOnlyList<SpielArgument> elements) : base(elements)
            {
            }
        }
    }
}
