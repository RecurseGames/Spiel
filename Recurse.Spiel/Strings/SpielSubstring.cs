﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Recurse.Spiel.English.Phrases
{
    /// <summary>
    /// A string of elements which will be concatenated with other elements
    /// to form a final string. It is assumed there is no whitespace at the
    /// start or end, making it clear that the following usage is appropriate:
    /// <code>
    /// new They(I) + Verb.Love + substring
    /// </code>
    /// </summary>
    [Serializable]
    public class SpielSubstring
    {
        [DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
        public IEnumerable<SpielArgument> Elements { get; }

        public SpielSubstring(params SpielArgument[] elements)
        {
            if (elements == null)
            {
                throw new ArgumentNullException(nameof(elements));
            }
            else if (elements.Length == 0)
            {
                throw new ArgumentException("Elements cannot be empty.", nameof(elements));
            }
            else if (elements.Select(e => e.Value).OfType<ISpielStringElement>().Count(e => e.IsRoot) > 1)
            {
                throw new ArgumentException("Elements cannot contain multiple roots.", nameof(elements));
            }

            Elements = elements;
        }

        private SpielSubstring(IEnumerable<SpielArgument> elements)
        {
            Elements = elements;
        }

        public static implicit operator SpielSubstring(string element)
        {
            if (element == null)
            {
                throw new ArgumentNullException(nameof(element));
            }

            return new SpielSubstring(element);
        }

        public static implicit operator SpielSubstring(SpielPartialString fragment)
        {
            return new SpielSubstring(fragment?.DelimitedElements ?? throw new ArgumentNullException(nameof(fragment)));
        }

        public static implicit operator SpielArgument(SpielSubstring composite)
        {
            return new SpielString(composite?.Elements ?? throw new ArgumentNullException(nameof(composite)));
        }

        public static SpielSubstring operator +(SpielSubstring left, SpielSubstring right)
        {
            var elements = Enumerable.Append(
                left?.Elements ?? throw new ArgumentNullException(nameof(left)),
                " ")
                .Concat(right?.Elements ?? throw new ArgumentNullException(nameof(right)));
            if (MultipleSubjects.Any(elements))
                throw new InvalidOperationException(MultipleSubjects.MultipleSubjectsExceptionMessage);
            return new SpielSubstring(elements);
        }

        public static SpielPartialString operator +(string left, SpielSubstring right)
        {
            return new SpielPartialString(
                left ?? throw new ArgumentNullException(nameof(left)),
                right?.Elements ?? throw new ArgumentNullException(nameof(right)));
        }

        public static SpielPartialString operator +(SpielSubstring left, string right)
        {
            return new SpielPartialString(
                left?.Elements ?? throw new ArgumentNullException(nameof(left)),
                right ?? throw new ArgumentNullException(nameof(right)));
        }

        public static SpielSubstring operator +(ISpielStringElement left, SpielSubstring right)
        {
            var elements = Enumerable.Prepend(
                right?.Elements ?? throw new ArgumentNullException(nameof(right)),
                " ")
                .Prepend(new SpielArgument(left ?? throw new ArgumentNullException(nameof(left))));
            if (MultipleSubjects.Any(elements))
                throw new InvalidOperationException(MultipleSubjects.MultipleSubjectsExceptionMessage);
            return new SpielSubstring(elements);
        }

        public static SpielSubstring operator +(SpielSubstring left, ISpielStringElement right)
        {
            var elements = Enumerable.Append(
                left?.Elements ?? throw new ArgumentNullException(nameof(left)),
                " ")
                .Append(new SpielArgument(right ?? throw new ArgumentNullException(nameof(right))));
            if (MultipleSubjects.Any(elements))
                throw new InvalidOperationException(MultipleSubjects.MultipleSubjectsExceptionMessage);
            return new SpielSubstring(elements);
        }
    }
}
