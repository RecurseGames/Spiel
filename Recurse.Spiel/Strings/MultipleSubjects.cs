﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Recurse.Spiel.English.Phrases
{
    public static class MultipleSubjects
    {
        public const string MultipleSubjectsExceptionMessage = "String cannot contain multiple roots.";

        public static bool Any(params ISpielStringElement[] elements)
        {
            return elements != null
                ? elements.Count(e => e.IsRoot) > 1
                : throw new ArgumentNullException(nameof(elements));
        }

        public static bool Any(IEnumerable<SpielArgument> elements)
        {
            return elements != null
                ? elements.Select(e => e.Value).OfType<ISpielStringElement>().Count(e => e.IsRoot) > 1
                : throw new ArgumentNullException(nameof(elements));
        }
    }
}
