﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Recurse.Spiel.English.Phrases
{
    /// <summary>
    /// A string of elements, usually a sentence or sentence clause, grouped into a single argument.
    /// </summary>
    [Serializable]
    public class SpielString : ISpielArgumentValue
    {
        [DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
        public IEnumerable<SpielArgument> Elements { get; }

        public SpielString(params SpielArgument[] elements)
        {
            Elements = SpielArgumentList.CreateNonEmpty(SpielArgumentListType.Sequence, elements)
                ?? throw new ArgumentException("Phrase needs at least 1 element.", nameof(elements));
        }

        public SpielString(IEnumerable<SpielArgument> elements)
        {
            Elements = SpielArgumentList.CreateNonEmpty(SpielArgumentListType.Sequence, elements)
                ?? throw new ArgumentException("Phrase needs at least 1 element.", nameof(elements));
        }

        public override string ToString()
        {
            return string.Concat(Elements.Select(e => e.ToRenderedString()));
        }

        public string ToRenderString()
        {
            return string.Concat(Elements.Select(e => e.ToRenderedString()));
        }

        public static implicit operator SpielArgument(SpielString phrase)
        {
            return new SpielArgument(phrase);
        }
    }
}
