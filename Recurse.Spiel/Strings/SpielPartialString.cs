﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Recurse.Spiel.English.Phrases
{
    /// <summary>
    /// <para>
    /// A string of elements, where it is unknown whether additional
    /// elements will be concatenated.
    /// </para>
    /// <para>
    /// Non-string elements are delimited by spaces for convenience.
    /// This allows strings to be constructed like so:
    /// <code>
    /// new They(I) + Verb.Love + new Them(You) + " very much";
    /// </code>
    /// rather than:
    /// <code>
    /// new They(I) + " " + Verb.Love + " " + new Them(You) + " very much";
    /// </code>
    /// </para>
    /// <para>
    /// <see cref="SpielPartialString"/>s are not intended to be concatenated
    /// directly with other elements, as this would cause confusion regarding
    /// proper insertion of spaces. Consider the possibilities:
    /// <code>
    /// var phrase1a = new They(Me) + ((SpielPartialString)" love"); // "I love"
    /// var phrase2a = new They(Me) + ((SpielPartialString)" love") + new Them(You); // "I loveyou"
    /// var phrase1b = new They(Me) + ((SpielPartialString)" love "); // "I love "
    /// var phrase2b = new They(Me) + ((SpielPartialString)" love ") + new Them(You); // "I love you"
    /// </code>
    /// As a result, before concatenating <see cref="SpielPartialString"/>s with other elements,
    /// they should be converted to type <see cref="SpielSubstring"/>.
    /// </para>
    /// </summary>
    [Serializable]
    public class SpielPartialString
    {
        private string LeftDelimiter { get; }

        private IReadOnlyCollection<SpielArgument> Elements { get; }

        private string RightDelimiter { get; }

        public IEnumerable<SpielArgument> DelimitedElements
        {
            get
            {
                IEnumerable<SpielArgument> result = Elements;
                if (LeftDelimiter != null)
                {
                    result = result.Prepend(LeftDelimiter);
                }
                if (RightDelimiter != null)
                {
                    result = result.Append(RightDelimiter);
                }
                return result;
            }
        }

        public SpielPartialString(ISpielStringElement element, string rightDelimiter)
        {
            Elements = SpielArgumentList.Create(
                SpielArgumentListType.Sequence,
                new SpielArgument(element ?? throw new ArgumentNullException(nameof(element))));
            RightDelimiter = rightDelimiter;
        }

        public SpielPartialString(string leftDelimiter, ISpielStringElement element)
        {
            LeftDelimiter = leftDelimiter;
            Elements = SpielArgumentList.Create(
                SpielArgumentListType.Sequence,
                new SpielArgument(element ?? throw new ArgumentNullException(nameof(element))));
        }

        public SpielPartialString(IEnumerable<SpielArgument> elements, string rightDelimiter)
        {
            Elements = SpielArgumentList.Create(SpielArgumentListType.Sequence, elements ?? throw new ArgumentNullException(nameof(elements)));
            RightDelimiter = rightDelimiter;
        }

        public SpielPartialString(string leftDelimiter, IEnumerable<SpielArgument> elements)
        {
            LeftDelimiter = leftDelimiter;
            Elements = SpielArgumentList.Create(SpielArgumentListType.Sequence, elements ?? throw new ArgumentNullException(nameof(elements)));
        }

        private SpielPartialString(string leftDelimiter, IReadOnlyCollection<SpielArgument> elements, string rightDelimiter)
        {
            LeftDelimiter = leftDelimiter;
            Elements = elements;
            RightDelimiter = rightDelimiter;
        }

        public static implicit operator SpielArgument(SpielPartialString elements)
        {
            var phrase = new SpielString(elements.DelimitedElements);
            return new SpielArgument(phrase);
        }

        public static SpielPartialString operator +(SpielPartialString left, SpielPartialString right)
        {
            var delimiter = GetDelimiter(
                left ?? throw new ArgumentNullException(nameof(left)),
                right ?? throw new ArgumentNullException(nameof(right)));
            var elements = SpielArgumentList.Create(SpielArgumentListType.Sequence, left.Elements.Append(delimiter).Concat(right.Elements));
            if (MultipleSubjects.Any(elements))
                throw new InvalidOperationException(MultipleSubjects.MultipleSubjectsExceptionMessage);
            return new SpielPartialString(left.LeftDelimiter, elements, right.RightDelimiter);
        }

        public static SpielPartialString operator +(string left, SpielPartialString right)
        {
            if (left == null)
                throw new ArgumentNullException(nameof(left));
            else if (right == null)
                throw new ArgumentNullException(nameof(right));
            return new SpielPartialString(left + right.LeftDelimiter, right.Elements, right.RightDelimiter);
        }

        public static SpielPartialString operator +(SpielPartialString left, string right)
        {
            if (left == null)
                throw new ArgumentNullException(nameof(left));
            else if (right == null)
                throw new ArgumentNullException(nameof(right));
            else
                return new SpielPartialString(left.LeftDelimiter, left.Elements, left.RightDelimiter + right);
        }

        public static SpielPartialString operator +(ISpielStringElement left, SpielPartialString right)
        {
            var elements = Enumerable.Prepend(
                right?.Elements ?? throw new ArgumentNullException(nameof(right)),
                right.LeftDelimiter ?? " ")
                .Prepend(new SpielArgument(left ?? throw new ArgumentNullException(nameof(left))));
            if (MultipleSubjects.Any(elements))
                throw new InvalidOperationException(MultipleSubjects.MultipleSubjectsExceptionMessage);
            return new SpielPartialString(elements, right.RightDelimiter);
        }

        public static SpielPartialString operator +(SpielPartialString left, ISpielStringElement right)
        {
            var elements = Enumerable.Append(
                left?.Elements ?? throw new ArgumentNullException(nameof(left)),
                left.RightDelimiter ?? " ")
                .Append(new SpielArgument(right ?? throw new ArgumentNullException(nameof(right))));
            if (MultipleSubjects.Any(elements))
                throw new InvalidOperationException(MultipleSubjects.MultipleSubjectsExceptionMessage);
            return new SpielPartialString(left.LeftDelimiter, elements);
        }

        private static SpielArgument GetDelimiter(SpielPartialString left, SpielPartialString right)
        {
            return left.RightDelimiter != null || right.LeftDelimiter != null
                ? left.RightDelimiter + right.LeftDelimiter
                : " ";
        }
    }
}
