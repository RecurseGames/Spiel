﻿namespace Recurse.Spiel.English.Phrases
{
#warning Document properly, separate into different module from IPhraseContextRenderable
    // Basically, an element which can be strung together with other elements to form a phrase
    public interface ISpielStringElement : ISpielArgumentValue
    {
        bool IsRoot { get; }
    }
}
