﻿using System;

namespace Recurse.Spiel.English.Phrases.Elements
{
    [Serializable]
    public abstract class SpielStringElement : ISpielStringElement, ISpielArgumentValue
    {
        public abstract bool IsRoot { get; }

        public abstract string ToRenderString();

        public override string ToString()
        {
            return $"{GetType().Name}({ToRenderString()})";
        }

        public static implicit operator SpielArgument(SpielStringElement element)
        {
            return new SpielArgument(element);
        }

        public static SpielSubstring operator +(SpielStringElement left, ISpielStringElement right)
        {
            if (MultipleSubjects.Any(
                left ?? throw new ArgumentNullException(nameof(left)),
                right ?? throw new ArgumentNullException(nameof(right))))
                throw new InvalidOperationException(MultipleSubjects.MultipleSubjectsExceptionMessage);
            return new SpielSubstring(left, " ", new SpielArgument(right));
        }

        public static SpielPartialString operator +(SpielStringElement left, string right)
        {
            return new SpielPartialString(
                left ?? throw new ArgumentNullException(nameof(left)),
                right ?? throw new ArgumentNullException(nameof(right)));
        }

        public static SpielPartialString operator +(string left, SpielStringElement right)
        {
            return new SpielPartialString(
                left ?? throw new ArgumentNullException(nameof(left)),
                right ?? throw new ArgumentNullException(nameof(right)));
        }
    }
}
