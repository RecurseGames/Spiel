﻿using System;
using System.Collections.Generic;

namespace Recurse.Spiel.Renderers
{
    /// <summary>
    /// Transforms sequences of linguistic elements into other linguistic
    /// elements. Typically, this process would be used to convert semantic
    /// concepts (e.g., a cow kissing a horse) into a form that can be
    /// rendered as text (e.g., "the cow kisses the horse").
    /// </summary>
    [Serializable]
    public class CompositeTransform
    {
        /// <summary>
        /// Transformations applied to an entire sequence of elements
        /// before each individual element is transformed.
        /// </summary>
        public IList<IArgumentsTransform> InitialSteps { get; } = new List<IArgumentsTransform>();

        /// <summary>
        /// Transformations applied to each individual element.
        /// </summary>
        public IList<IArgumentTransform> ElementRenderers { get; } = new List<IArgumentTransform>();

        /// <summary>
        /// Transformations applied to an entire sequence of elements
        /// after each individual element is transformed.
        /// </summary>
        public IList<IArgumentsTransform> FinalSteps { get; } = new List<IArgumentsTransform>();

        public IEnumerable<SpielArgument> GetTransformedArguments(SpielArgumentList arguments)
        {
            if (arguments == null)
            {
                throw new ArgumentNullException(nameof(arguments));
            }

            foreach (var step in InitialSteps)
            {
                var renderedArguments = SpielArgumentList.Create(arguments.Type, step.TransformArguments(arguments));

                arguments = renderedArguments;
            }

            var renderedElements = SpielArgumentList.Create(arguments.Type);
            foreach (var initialArgument in arguments)
            {
                var argument = initialArgument;
                foreach (var elementRenderer in ElementRenderers)
                {
                    var renderedArgument = elementRenderer.TransformArgument(argument);

                    argument = renderedArgument;
                }

                renderedElements.Add(argument);
            }
            arguments = renderedElements;

            foreach (var step in FinalSteps)
            {
                var renderedArguments = SpielArgumentList.Create(arguments.Type, step.TransformArguments(arguments));

                arguments = renderedArguments;
            }

            return arguments;
        }
    }
}
